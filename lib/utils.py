import sys
import os.path
import json
from configparser import ConfigParser


def get_config():
    configFile = 'config/config.ini'
    #configFile = ''.join([os.path.dirname(__file__), '/', configFile])

    if not os.path.isfile(configFile):
        raise IOError('Could not read configuration file %s' % configFile)

    config = ConfigParser()
    config.read(configFile)
    return config


def progress_bar(count, total):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + ' ' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s\r' % (bar, percents, '%'))
    sys.stdout.flush()
    if(count == total):
        print("\n")


def get_nodes_attributes_config():
    with open('config/nodes_attributes_config.json') as data_file:
        return json.load(data_file)


def get_links_attributes_config():
    with open('config/links_attributes_config.json') as data_file:
        return json.load(data_file)

