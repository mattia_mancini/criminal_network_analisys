import neo4j

from lib.utils import get_config
from lib import logger

def neo4jInit():
    config = get_config()

    # Neo4j Connection
    # Remote
    connection = neo4j.connect(config.get('Neo4jLegacy', 'connection_string'))
    if config.has_option('Neo4jLegacy', 'user'):
        connection.authorization(config.get('Neo4jLegacy', 'user'), config.get('Neo4jLegacy', 'password'))

    logger.getLogger().info("Connected to Neo4j @ %s" % config.get('Neo4jLegacy', 'connection_string'))
    return connection

def deleteAll(neo4j_connection):
    cursor = neo4j_connection.cursor()
    logger.getLogger().info("Delete all nodes and relations...")
    d = cursor.execute("MATCH (a) OPTIONAL MATCH (a)-[r]-() WITH a,r DELETE a,r RETURN count(a) as deletedNodesCount")
    neo4j_connection.commit()
