import neo4j
import geocoder
import json
from lib import logger

def getNodesWithoutCoordinates(neo4j_connection):
    matchingNodes = []
    q = "MATCH (n) WHERE has(n.indirizzocompleto) AND (not has(n.latitude) OR not has(n.longitude)) RETURN n"

    for p in neo4j_connection.cursor().execute(q):
        matchingNodes.append(p[0])

    return matchingNodes

def getNodesWithCoordinates(neo4j_connection):
    matchingNodes = []
    q = "MATCH (n)-[r]-(d:documento) WHERE has(n.indirizzocompleto) AND has(n.latitude) AND has(n.longitude) RETURN n"

    for p in neo4j_connection.cursor().execute(q):
        matchingNodes.append(p[0])

    return matchingNodes

def geocode(nodes, neo4j_connection):
    i = 0
    for n in nodes:
        if 'indirizzocompleto' in n:
            g = geocoder.google(n['indirizzocompleto'])

            if len(g.latlng) == 0:
                g = geocoder.osm(n['indirizzocompleto'])

            if len(g.latlng) == 0:
                g = geocoder.arcgis(n['indirizzocompleto'])

            if len(g.latlng) == 0:
                logger.getLogger().info(("Errore nella localizzazine del nodo :%s - %s" % (g.status, json.dumps(n))))
            else:
                lat, lng = g.latlng
                logger.getLogger().info(("node %s [%s, %s], %s" % (n.id, lat, lng, json.dumps(n))))
                neo4j_connection.cursor()\
                    .execute("MATCH (n) WHERE ID(n)=%s SET n.latitude=%s, n.longitude=%s" % (n.id, lat, lng))
                i += 1
        else:
            logger.getLogger().error(("Errore %s non ha il campo 'indirizzocompleto'" % json.dumps(n)))

        if i % 100 == 0:
            neo4j_connection.commit()

    neo4j_connection.commit()



