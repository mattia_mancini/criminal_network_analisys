import hashlib
import os.path
import pickle
from lib import logger
from lib.utils import get_config

def cache(cachekey, dataprovider, *args):
    cachedir = get_config().get('Cache', 'directory')
    cachefile = "{0}/{1}.pickle".format(cachedir, __getHash(cachekey))

    if os.path.isfile(cachefile):
        with open(cachefile, 'rb') as f:
            ds = pickle.load(f)
        logger.getLogger().info("Loaded data from cachefile ({0})".format(cachefile))
    else:
        ds = dataprovider(*args)
        os.makedirs(cachedir, exist_ok=True)
#        with open(cachefile, 'wb') as f:
#            pickle.dump(ds, f)
        logger.getLogger().info("Saved data to cachefile ({0})".format(cachefile))

    return ds

def __getHash(n):
    return hashlib.sha256(n.encode('utf-8')).hexdigest()
