from lib import datamanager_legacy
from nltk.tokenize import *
from nltk.metrics.distance import edit_distance
from fuzzywuzzy import fuzz
import hashlib
import pickle
import json
import csv
import multiprocessing as mp
from multiprocessing import sharedctypes
from lib import logger
import numpy as np
import re

from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
import scipy.spatial.distance as ssd
from scipy.cluster.hierarchy import fcluster

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False

def normalize(s):
    s = s.replace("'", "")
    s = s.replace(".", "")
    s = s.replace("-", " ")
    s = s.replace("_", " ")
    return s

def getNameUniqueHash(n):
    uniquestring = ' '.join((n['drereference'], str(n['entitaid']))).encode('utf-8').strip()
    return hashlib.sha256(uniquestring).hexdigest()

def processType(name, tipogruppo):
    cat_1 = ["hotel","albergo","residence","motel",]
    cat_2 = ["famiglia mafiosa", "famiglia", "cosca mafiosa", "organizzazione mafiosa",
             "mandamento", "clan", "associazione criminale", "associazione mafiosa",
             "organizzazione criminale", "organizzazione", "cosca"]
    cat_3 = ["loggia massonica", "loggia"]
    cat_4 = ["magione"]

    for i in range(0, len(cat_1)):
        if cat_1[i] in name.lower() or cat_1[i] == tipogruppo:
            src_str  = re.compile(cat_1[i], re.IGNORECASE)
            name = src_str.sub("", name)
            name = name.rstrip().lstrip().replace("  ", " ")
            return name, 1

    for i in range(0, len(cat_2)):
        if cat_2[i] in name.lower() or cat_2[i] == tipogruppo:
            src_str  = re.compile(cat_2[i], re.IGNORECASE)
            name = src_str.sub("", name)
            name = name.rstrip().lstrip().replace("  ", " ")
            return name, 2

    for i in range(0, len(cat_3)):
        if cat_3[i] in name.lower() or cat_3[i] == tipogruppo:
            src_str  = re.compile(cat_3[i], re.IGNORECASE)
            name = src_str.sub("", name)
            name = name.rstrip().lstrip().replace("  ", " ")
            return name, 3

    for i in range(0, len(cat_4)):
        if cat_4[i] in name.lower() or cat_4[i] == tipogruppo:
            src_str  = re.compile(cat_4[i], re.IGNORECASE)
            name = src_str.sub("", name)
            name = name.rstrip().lstrip().replace("  ", " ")
            return name, 3

    return name, -1

def getNodes(neo4j_connection, tag):
    # q = "MATCH (n:persona) RETURN n"
    # q = "MATCH (n:persona) RETURN n LIMIT 100"
    # q = "MATCH (n:persona)-[a]-(d:documento) WHERE d.regione='Campania' RETURN n"
    # q = "MATCH (n) WHERE n.tag='individuo' OR n.tag='gruppo' RETURN n"
    q = "MATCH (n) WHERE n.tag='%s' RETURN n ORDER BY n.nominativo ASC" % tag

    matrix_id = 0
    r = []
    for p in neo4j_connection.cursor().execute(q):
        sesso = ''
        via = ''
        comune = ''
        regione = ''
        name_0 = ''
        name_1 = ''
        _type = None

        if tag == 'persona':
            name = normalize(p[0]['nominativo'])
            try :
                name_0 = normalize(p[0]['nome'])
            except :
                name_0 = ''
            try :
                name_1 = normalize(p[0]['cognome'])
            except :
                name_1 = ''
            try :
                sesso = normalize(p[0]['sesso'])
            except :
                sesso = ''

        elif tag == 'organizzazione':
            name = normalize(p[0]['denominazione'])
            try :
                tipogruppo = p[0]['tipogruppo']
            except:
                tipogruppo = ''
            name_0,_type = processType(name,tipogruppo)
            try :
                via = normalize(p[0]['via'])
            except :
                via = ''
            try :
                comune = normalize(p[0]['comune'])
            except :
                comune = ''
            try :
                regione = normalize(p[0]['regione'])
            except :
                regione = ''
        else:
            raise RuntimeError("Tag %s non valido" % tag)

        r.append({
            'id':           p[0]['internal_id'],
            'matrix_id':    matrix_id,
            'nome_0':       name_0,
            'nome_1':       name_1,
            'nominativo':   name,
            'sesso' :       sesso,
            'via':          via,
            'comune':       comune,
            'regione':      regione,
            'uniqid':       getNameUniqueHash(p[0]),
            'node':         p[0],
            'type':         _type
        })

        matrix_id += 1
    return r


def getNameByIndex(names, matrix_id):
    return next((person for person in names if person["matrix_id"] == matrix_id), None)

def getNameByUniqid(names, uniqid):
    return next((person for person in names if person["uniqid"] == uniqid), None)

def editDistance(s1, s2):
    return edit_distance(s1, s2)

def mongoGetConnection():
    try:
        neo4j_connection = datamanager_legacy.neo4jInit()
        return neo4j_connection
    except Exception as e:
        print("Neo4j error: %s" % str(e))
        sys.exit(1)

def distanceFuzzyPersona(s1, s2, neo4j_connection):

    c1 = s1['nome_1']
    c2 = s2['nome_1']

    if(c1[0:1] != c2[0:1]):
        return 100

    if s1['sesso'] is not s2['sesso']:
        return 100

    if s1['nome_0'] is '' or s2['nome_0'] is '' or s1['nome_1'] is '' or s2['nome_1'] is '':
        return 100

    q = "MATCH (n:persona)-[r]-(m:documento) WHERE n.internal_id=%s OR n.internal_id=%s RETURN m.internal_id" %( s1['id'], s2['id'])
    p_doc_id = [0,0]
    i = 0
    for p in neo4j_connection.cursor().execute(q):
        p_doc_id[i] = p[0]
        i += 1

    doc_diff = 0
    if p_doc_id[0] != p_doc_id[1] :
        doc_diff = 5

    diff_len = abs(len(s1['nominativo']) - len (s2['nominativo']))
    if diff_len > 2:
        diff_len = (diff_len - 2) * 5
    else:
        diff_len = 0

    name_similarity = (fuzz.ratio(s1['nome_0'], s2['nome_0']) + fuzz.partial_ratio(s1['nome_0'], s2['nome_0'])
                        + fuzz.token_sort_ratio(s1['nome_1'], s2['nome_1']))/3
    surname_similarity = (fuzz.ratio(s1['nome_1'], s2['nome_1']) + fuzz.partial_ratio(s1['nome_1'], s2['nome_1'])
                          + fuzz.token_sort_ratio(s1['nome_1'], s2['nome_1']))/3

    if (s1['nome_0'] is '' and s2['nome_0'] is '') or (s1['nome_1'] is '' and s2['nome_1'] is ''):
        if (" " in s1['nome_0'] and " " in s2['nome_0']) or (" " in s1['nome_1'] and " " in s2['nome_1']):
            if "." in s1['nominativo'] or "." in s2['nominativo']:
                return 100

            nominativo_similarity = fuzz.token_sort_ratio(s1['nominativo'], s2['nominativo'])
            if nominativo_similarity >= 90 :
                return 0 + doc_diff + diff_len
            elif nominativo_similarity >= 80 :
                return 10 + doc_diff + diff_len;
            elif nominativo_similarity <= 35 :
                return 100
            else:
                return 100 - nominativo_similarity + doc_diff + diff_len
        else :
            return 100

    if (name_similarity >= 90 and surname_similarity >= 90):
        return 0 + doc_diff + diff_len
    elif (name_similarity >= 80 and surname_similarity >= 80):
        return 10 + doc_diff + diff_len
    elif (name_similarity <= 35 or surname_similarity <= 35):
        return 100
    else:
        d = (name_similarity + surname_similarity) / 2
        if d < 50:
            return 100
        else :
            return 100 - d + doc_diff + diff_len

def processStrings(s1,s2):
    splitted_1 = s1.split()
    splitted_2 = s2.split()

    to_remove = ["di","a","da","in","con","su","per","tra","fra", "il","lo","la",
                 "i","gli","le","un","uno","una","srl", "sas", "spa", "snc", "dei"]

    for i in range(0, len(splitted_1)):
        for word in to_remove:
            if word == splitted_1[i].lower():
                splitted_1[i] = ''
                break

    for i in range(0, len(splitted_2)):
        for word in to_remove:
            if word == splitted_2[i].lower():
                splitted_2[i] = ''
                break

    clean_s1 = ' '.join([w for w in splitted_1]).rstrip().lstrip().replace("  ", " ")
    clean_s2 = ' '.join([w for w in splitted_2]).rstrip().lstrip().replace("  ", " ")
    return clean_s1, clean_s2

#Calcolo della distanza per le organizzazioni
def distanceFuzzyOrganizzazione(s1, s2, neo4j_connection):

    s1['nome_0'],s2['nome_0'] = processStrings(s1['nome_0'],s2['nome_0'])

#    if s1['type'] != s2['type']:
#        if s1['type'] != -1 and s2['type'] != -1:
#            return 100

    q = "MATCH (n:organizzazione)-[r]-(m:documento) WHERE n.internal_id=%s OR n.internal_id=%s RETURN m.internal_id" %( s1['id'], s2['id'])
    p_doc_id = [0,0]
    i = 0
    for p in neo4j_connection.cursor().execute(q):
        p_doc_id[i] = p[0]
        i += 1

    doc_diff = 0
    if p_doc_id[0] != p_doc_id[1] :
        doc_diff = 5

    if len(s1['nome_0']) > 5 and len(s2['nome_0']) > 5:
        similarity_max = max(fuzz.partial_ratio(s1['nome_0'], s2['nome_0']),
                                              fuzz.token_sort_ratio(s1['nome_0'], s2['nome_0']))
    else:
        similarity_max = (fuzz.partial_ratio(s1['nome_0'], s2['nome_0'])
                                       + fuzz.token_sort_ratio(s1['nome_0'], s2['nome_0'])) / 2

    if s1['regione'] == s2['regione'] and s1['regione'] is not '':
        similarity_max += 0.5
    if s1['comune'] == s2['comune'] and s1['comune'] is not '':
        similarity_max += 1
    if s1['via'] == s2['via'] and s1['via'] is not '':
        similarity_max += 5

    similarity_max -= doc_diff

    if similarity_max >= 90:
        return 0
    elif similarity_max <= 35:
        return 100
    else:
        return 100 - similarity_max

def distanceSplittingNames(s1, s2, distance, *args):
    if len(s1) >= len(s2):
        s1_pieces = wordpunct_tokenize(s1)
        s2_pieces = wordpunct_tokenize(s2)
    else:
        s1_pieces = wordpunct_tokenize(s2)
        s2_pieces = wordpunct_tokenize(s1)

    totalDistance = 0

    for i in s1_pieces:
        wordbest = len(i)
        for j in s2_pieces:
            d = distance(i, j, *args)
            if d < wordbest:
                wordbest = d
            if d == 0:
                break
        totalDistance += wordbest
    return totalDistance

def calculateDistance(names, distance_function, *args):
    m = np.zeros(shape=(len(names), len(names)))
    count = 0
    for i in range(0, len(names)):
        n1 = getNameByIndex(names, i)
        for j in range(i, len(names)):
            n2 = getNameByIndex(names, j)
            d = distance_function(n1['nominativo'], n2['nominativo'], *args)
            m[i, j] = d
            m[j, i] = d
            count += 1

    return m

def calculateDistanceMultiproc(names, tag, *args):
    neo4j_connection = mongoGetConnection()
    log = logger.getLogger()
    def worker(queue, shared_arr, *args):
        while True:
            task = queue.get()
            if task is None:
                queue.task_done()
            if task['n1']['matrix_id'] == task['n2']['matrix_id']:
                d = 0
            else:
                if tag is "persona":
                    d = distanceFuzzyPersona(task['n1'], task['n2'], neo4j_connection, *args)
                else:
                    d = distanceFuzzyOrganizzazione(task['n1'], task['n2'], neo4j_connection, *args)

            i = task['idx1']
            j = task['idx2']
            shared_arr[i][j] = d
            shared_arr[j][i] = d
            queue.task_done()
            # log.debug("|%d, %d| = %d" % (task['idx1'], task['idx2'], d))
        return

    work_queue = mp.JoinableQueue(maxsize=8000)
    num_consumers = mp.cpu_count() * 2
    log.debug("Starting %d processes..." % num_consumers)
    # Faccio un array nodi * nodi, lo setto a zero e ritorno l'oggetto dove salvare i dati
    tmp = np.ctypeslib.as_ctypes(np.zeros(shape=(len(names), len(names))))
    # Rendo l'array condiviso tra i vari thread
    shared_arr = sharedctypes.Array(tmp._type_, tmp, lock=False)

    for w in range(num_consumers):
        p = mp.Process(target=worker, args=(work_queue, shared_arr, *args, ))
        p.daemon = True
        p.start()

    log.debug("Setting up the multiprocessing queue...")
    for i in range(0, len(names)):
        n1 = getNameByIndex(names, i)
        for j in range(i, len(names)):
            n2 = getNameByIndex(names, j)
            work_queue.put({'n1': n1, 'n2': n2, 'idx1': i, 'idx2': j})

    log.debug("Calculating distances...")
    work_queue.join()
    final_arr = np.ctypeslib.as_array(shared_arr)

    log.debug("Distance matrix done!")
    neo4j_connection.close()
    return final_arr

def plot(m, names, title, filename):
    names_string = [n['nominativo'] for n in names]

    fig, ax = plt.subplots()
    # ax.set_title(title)
    ax.tick_params(labelsize=0.9)
    heatmap = ax.pcolor(m, cmap=plt.cm.YlOrRd_r)

    # Format
    size = max(len(names) * (1 / 70), 3)
    fig = plt.gcf()
    fig.set_size_inches(size, size)

    # turn off the frame
    ax.set_frame_on(False)

    # put the major ticks at the middle of each cell
    ax.set_yticks(np.arange(m.shape[0]) + 0.5, minor=False)
    ax.set_xticks(np.arange(m.shape[1]) + 0.5, minor=False)

    ax.invert_yaxis()
    ax.xaxis.tick_top()

    plt.xticks(rotation=90)

    ax.set_xticklabels(names_string, minor=False)
    ax.set_yticklabels(names_string, minor=False)

    #fig.colorbar(heatmap)
    plt.savefig(filename, format='pdf')

def getTopMergiableCandidates(m, names, max_distance):
    distances = []
    for i in range(0, len(names)):
        for j in range(i+1, len(names)):
            n1 = getNameByIndex(names, i)
            n2 = getNameByIndex(names, j)
            if m[i, j] <= max_distance:
                distances.append((m[i, j], n1, n2))
    distances.sort(key=lambda tup: tup[0])
    return distances

def getInteractiveMergeList(m, names, max_distance):
    candidates = getTopMergiableCandidates(m, names, max_distance)
    mergeList = []
    endProcess = False
    for candidate in candidates:
        if endProcess:
            break
        n1 = "%s [%s]" % (candidate[1]['nominativo'], candidate[1]['id'])
        n2 = "%s [%s]" % (candidate[2]['nominativo'], candidate[2]['id'])
        distance = candidate[0]
        print(("%s => %s \t (%d)" % (n1, n2, distance)))

        while True:
            userInput = eval(input("Merge (m), link (l) or skip (s)? (q to end process): "))
            if userInput == 'm' or userInput == '<' or userInput == '>' or userInput == 'l':
                operation = {'n1': candidate[1], 'n2': candidate[2], 'operation': userInput}
                print(operation)
                mergeList.append(operation)
                break
            elif userInput == 's':
                print("skip")
                break
            elif userInput == 'q':
                print("end process")
                endProcess = True
                break
            else:
                print("Invalid input")

    return mergeList

def writeMergeList(m, names, max_distance, filecsv):
    with open(filecsv, 'wb') as csvfile:
        writer = csv.writer(csvfile)
        candidates = getTopMergiableCandidates(m, names, max_distance)
        for candidate in candidates:
            if candidate[1]['nominativo'] == 'ia' or candidate[1]['nominativo'] == 'il' or candidate[2]['nominativo'] == 'ia' or candidate[2]['nominativo'] == 'il':
                continue
            n1 = ' '.join((candidate[1]['nominativo'], str(candidate[1]['id']))).encode('utf-8').strip()
            n2 = ' '.join((candidate[2]['nominativo'], str(candidate[2]['id']))).encode('utf-8').strip()
            writer.writerow([candidate[1]['uniqid'], candidate[2]['uniqid'], n1, n2, 's'])

def loadMergeList(names, filecsv):
    mergeList = []
    with open(filecsv, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            n1 = getNameByUniqid(names, row[0])
            n2 = getNameByUniqid(names, row[1])
            op = row[4]
            operation = {'n1': n1, 'n2': n2, 'operation': op}
            mergeList.append(operation)
    return mergeList


# def joinMergeLists(mergeList1, mergeList2):
#     joinlist = mergeList1
#     for i in mergeList2:
#         for j in joinlist:
#             if cmp(i['n1'], j['n1']) != 0 and cmp(i['n1'], j['n2']) != 0 and cmp(i['n2'], j['n1']) != 0:
#                 joinlist.append(j)
#     return joinlist

def flattenEntity(entity):
    partialKeyMap = []
    for (key, val) in list(entity.items()):

        if val is None:
            continue
        if (isinstance(val, str) or isinstance(val, str)) and is_number(val):
            value = "%s: %f" % (key, float(val))
        elif isinstance(val, int):
            value = "%s: %d" % (key, val)
        else:
            value = "%s: %s" % (key, json.dumps(val))

        partialKeyMap.append(value)

    return ', '.join(partialKeyMap)

def delete_campania(neo4j_connection):
    cursor = neo4j_connection.cursor()
    cursor.execute("MATCH (n) WHERE n.tag='individuo' OR n.tag='gruppo' DETACH DELETE n")
    neo4j_connection.commit()

def insertRelationshipString(n1_id, n2_id, rel):
    relType = rel.type
    for prop in ['start_id', 'end_id', 'id', 'type']:
        if prop in rel: del rel[prop]

    q = "MATCH (sFrom), (sTo) " \
        "WHERE sFrom.internal_id=%d AND sTo.internal_id=%d " \
        "MERGE (sFrom)-[:%s {%s}]->(sTo)" % \
        (n1_id, n2_id, relType, flattenEntity(rel))

    return q

def linkNodesRelationship(n1_id, n2_id):
    # TODO: Aggiungere nella relazione la lista dei drereference dei nodi collegati
    relType = "possible_duplicate"

    q = "MATCH (sFrom), (sTo) " \
        "WHERE sFrom.internal_id=%d AND sTo.internal_id=%d " \
        "MERGE (sTo)-[:%s {tag:'%s'}]->(sFrom)" % \
        (n1_id, n2_id, relType, relType)

    return q


def mergeNodes(n1, n2, neo4j_connection):
    logger.getLogger().info("Merging %s <= %s" % (n1['nominativo'], n2['nominativo']))

    cursor = neo4j_connection.cursor()
    q = "MATCH (n2)-[r]-(m) WHERE n2.internal_id=%d AND m.internal_id<>%d RETURN n2,r,m" % (n2['id'], n1['id'])

    node_from_id = n1['id']

    if 'appended_nodes' not in n1:
        n1['appended_nodes'] = [n1['id']]
        neo4j_connection.cursor().execute("MATCH (n1) WHERE n1.internal_id=%d SET n1.appended_nodes = [%d] RETURN n1" % (n1['id'], n1['id']))
        neo4j_connection.commit()

    n1['appended_nodes'].append(n2['id'])
    neo4j_connection.cursor().execute("MATCH (n1) WHERE n1.internal_id=%d SET n1.appended_nodes = n1.appended_nodes + %d RETURN n1" % (n1['id'], n2['id']))
    neo4j_connection.commit()

    for deletable_node_relationship in neo4j_connection.cursor().execute(q):
        relationship = deletable_node_relationship[1]
        node_to = deletable_node_relationship[2]
        node_to_id = int(node_to['internal_id'])
        insert_string = insertRelationshipString(node_from_id, node_to_id, relationship)
        cursor.execute(insert_string)
    delete_string = "MATCH (n2) WHERE n2.internal_id=%d DETACH DELETE n2" % n2['id']
    cursor.execute(delete_string)
    neo4j_connection.commit()
    logger.getLogger().info("Merge done, deleted node %s" % n2['id'])


def possibleDuplicateNodes(n1, n2, neo4j_connection):
    q = "MATCH (sFrom)-[r:possible_duplicate]-(sTo) " \
            "WHERE sFrom.internal_id=%d AND sTo.internal_id=%d " \
            "RETURN COUNT(r)" % (n1['id'], n2['id'])
    for count in neo4j_connection.cursor().execute(q):
        if count[0] > 0:
            logger.getLogger().info("Already linked nodes %s <=> %s" % (n1['nominativo'], n2['nominativo']))
            return

    logger.getLogger().info("Linking possible duplicate %s <=> %s" % (n1['nominativo'], n2['nominativo']))

    cursor = neo4j_connection.cursor()
    linking_string = linkNodesRelationship(n1['id'], n2['id'])
    cursor.execute(linking_string)
    neo4j_connection.commit()

def applyMergeList(mergeList, neo4j_connection):
    # TODO: Aggiungere nel nodo fuso la lista dei drereference dei nodi eliminati
    q = "MATCH (n1:individuo)-[r]-(n2) WHERE n1.internal_id=%d AND n2.internal_id<>%d RETURN n1,r,n2"

    cursor = neo4j_connection.cursor()

    merging_operations = 0
    linking_operations = 0
    deleting_operations = 0

    for operation in mergeList:
        # Merge duplicated nodes
        if operation['operation'] == 'm' or operation['operation'] == '<' or operation['operation'] == '>':
            print(("Merging %s %s %s" % (operation['n1']['nominativo'], operation['operation'], operation['n2']['nominativo'])))

            if operation['operation'] == 'm' or operation['operation'] == '<':
                n1 = operation['n1']
                n2 = operation['n2']
            else:
                n2 = operation['n1']
                n1 = operation['n2']

            q2 = q % (n2['id'], n2['id'])

            node_from_id = n1['id']
            for deletable_node_relationship in neo4j_connection.cursor().execute(q2):
                relationship = deletable_node_relationship[1]
                node_to = deletable_node_relationship[2]
                node_to_id = int(node_to['internal_id'])
                insert_string = insertRelationshipString(node_from_id, node_to_id, relationship)
                cursor.execute(insert_string)
                merging_operations += 1
            delete_string = "MATCH (n2:individuo) WHERE n2.internal_id=%d DETACH DELETE n2" % n2['id']
            cursor.execute(delete_string)
            deleting_operations += 1
            neo4j_connection.commit()

        # Link nodes with strong similarity (possible duplicates)
        elif operation['operation'] == 'l':
            print(("Linking %s with %s" % (operation['n1']['nominativo'], operation['n2']['nominativo'])))
            linking_string = linkNodesRelationship(operation['n1']['id'], operation['n2']['id'])
            cursor.execute(linking_string)
            linking_operations += 1
            neo4j_connection.commit()
        else:
            print(("Skipping %s with %s" % (operation['n1']['nominativo'], operation['n2']['nominativo'])))

    print(("Deleted %d duplicated nodes, moved %d relationships, linked %d similar pair of nodes..." %
           (deleting_operations, merging_operations, linking_operations)))


def saveAsset(asset, file):
    pickle.dump(asset, open(file, "wb"))

def loadAsset(file):
    asset = pickle.load(open(file, "r"))
    return asset

def report(m, names, filename, max_distance):
    candidates = getTopMergiableCandidates(m, names, max_distance)

    with open(filename, "w") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for candidate in candidates:
            # print "%s => %s \t (%d)" % (candidate[1]['nominativo'], candidate[2]['nominativo'], candidate[0])
            n1 = "%s [%s]" % (candidate[1]['nominativo'], candidate[1]['id'])
            n2 = "%s [%s]" % (candidate[2]['nominativo'], candidate[2]['id'])
            distance = candidate[0]
            row = [n1, n2, distance]
            writer.writerow([str(s).encode("utf-8") for s in row])

def clusteringDendrogramPlot(distanceMatrix, names, method='complete', threshold=10, filename='results/Dendrogram'):
    distArray = ssd.squareform(distanceMatrix)
    n = [a['nominativo'] for a in names]
    clusters = linkage(distArray, method)

    plt.figure()
    plt.title('Hierarchical Clustering Dendrogram')
    plt.ylabel('names')
    plt.xlabel('distance')
    dend = dendrogram(
        clusters,
        #leaf_rotation=45.,  # rotates the x axis labels
        leaf_font_size=11.,  # font size for the x axis labels
        labels=n,
        orientation='right'
    )
    plt.axvline(x=threshold, c='k')
    plt.axvline(x=10, color='r')
    size = len(names) * (12 / 70)
    fig = plt.gcf()
    fig.set_size_inches(20, size)
    plt.savefig(filename, format='pdf')

def clustering(distanceMatrix, names, tag, method='complete', threshold=10, min_cluster_items=2):

    distArray = ssd.squareform(distanceMatrix)
    clusteringResult = {}
    clusteringToPrint={}
    n = range(0, len(names)-1)
    #n = [ a['nominativo'] for a in names]
    clusters = linkage(distArray, method)
    assignments = fcluster(clusters, threshold, 'distance')
    result = sorted(list(zip(n, assignments)), key=lambda x: x[0])

    for r in result:
        name_index = r[0]
        cluster_id = r[1]
        clusteringResult[cluster_id] = clusteringResult.get(cluster_id, [])
        clusteringToPrint[cluster_id] = clusteringToPrint.get(cluster_id, [])

        if tag is "persona":
            clusteringResult[cluster_id].append(names[name_index])
            clusteringToPrint[cluster_id].append(names[name_index]['node']['nominativo'])
        else:
            clusteringResult[cluster_id].append(names[name_index])
            clusteringToPrint[cluster_id].append(names[name_index]['node']['denominazione'])

    print("\nClustering Result: \n\n", clusteringToPrint)

    return {cluster_id: item_list for cluster_id, item_list in clusteringResult.items() if len(item_list) >= min_cluster_items}

def intraclusterDistance(listOfNodes, distanceMatrix):
    n = 0
    d = 0
    for i in listOfNodes:
        for j in listOfNodes:
            if i['matrix_id'] != j['matrix_id']:
                temp = distanceMatrix[i['matrix_id']][j['matrix_id']]
                d += temp
                n += 1
    return len(listOfNodes), d / n

def mergeNodesInClusters(clusters, distanceMatrix, neo4j_connection, mergeThreshold=10, apply=False):
    log = logger.getLogger()

    for cluster_id, nodelist in clusters.items():
        dimCluster, d = intraclusterDistance(nodelist, distanceMatrix)
        #log.info("- Cluster #%d: intra-cluster mean distance: %0.2f -" % (cluster_id, d))
        print("- Cluster #%d: intra-cluster mean distance: %0.2f -" % (cluster_id, d))

        masternode = None

        toMergeList = []
        if (d > mergeThreshold and dimCluster > 2) :
            print("* Chosen policy: Selective Merge")
            print("* Applying selective merge on cluster: ", cluster_id)
            for i in nodelist:
                currentRow = []
                for j in nodelist:
                    if i['matrix_id'] != j['matrix_id']:
                        if distanceMatrix[i['matrix_id']][j['matrix_id']] <= mergeThreshold:
                            check_in_element = False
                            for elem in toMergeList:
                                for node in elem:
                                    if i['id'] is node['id'] or j['id'] is node['id']:
                                        check_in_element = True
                                        if i['id'] is node['id']:
                                            found_j = False
                                            for n in elem:
                                                if j['id'] is n['id']:
                                                    found_j = True
                                            if found_j is False:
                                                elem.append(j)
                                            #break
                                        else :
                                            found_i = False
                                            for n in elem:
                                                if i['id'] is n['id']:
                                                    found_i = True
                                            if found_i is False:
                                                elem.append(i)
                            if check_in_element is False:
                                currentRow.append(j)
                toMergeList.append(currentRow)

            for elem in toMergeList:
                for node in elem:
                    if not masternode or len(node['node']) > len(masternode['node']):
                        masternode = node

                if masternode is not None:
                    print("==> master node id: %d" % (masternode['id']))

                for node in elem:
                    if node['id'] != masternode['id']:
                        if apply:
                            print ("Saving merge on DB")
                            mergeNodes(masternode, node, neo4j_connection)
                        else:
                            log.info("(merging not applied, by configuration)")
                masternode = None

        elif (d <= mergeThreshold):
            for node in nodelist:
                if not masternode or len(node['node']) > len(masternode['node']):
                    masternode = node
#                log.info("\t. %s [%d] \t %s" % (node['nominativo'], node['id'], node['node']))

                print("* Chosen policy: Normal Merge")
                print("* Applying normal merge on cluster: ", cluster_id)
                print("==> master node id: %d" % (masternode['id']))
                for node in nodelist:
                    if node['id'] != masternode['id']:
                        if apply:
                            print ("Saving merge on DB")
                            mergeNodes(masternode, node, neo4j_connection)
                        else:
                            log.info("(merging not applied, by configuration)")
        else:
            log.info("* chosen policy: Possible duplicate")
            for n1 in nodelist:
                for n2 in nodelist:
                    if n1['id'] != n2['id']:
                        if apply:
                            possibleDuplicateNodes(n1, n2, neo4j_connection)
                        else:
                            log.info("(duplicate relations not applied, by configuration)")
