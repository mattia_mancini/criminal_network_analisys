__author__ = 'fdellutri'
import logging, os

_logger = None


def getLogger(name=None):
    global _logger

    if not name and not _logger:
        raise NameError('You must specify the name for logger')

    if _logger:
        return _logger

    name = os.path.basename(name)
    _logger = logging.getLogger(name)

    _logger.setLevel(logging.DEBUG)

    # create file handler which logs even debug messages
    fh = logging.FileHandler('logs/%s.log' % name)
    fh.setLevel(logging.DEBUG)

    # create file error handler with a higher log level
    feh = logging.FileHandler('logs/%s.log' % name)
    feh.setLevel(logging.ERROR)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s %(processName)-12s %(levelname)-8s %(message)s')

    fh.setFormatter(formatter)
    feh.setFormatter(formatter)
    ch.setFormatter(formatter)

    # add the handlers to the logger
    _logger.addHandler(fh)
    _logger.addHandler(feh)
    _logger.addHandler(ch)

    return _logger
