import hashlib
import os.path
import pickle
import json
import sys
import operator
import random

from py2neo import Graph as Py2neoGraph
from igraph import *
from copy import deepcopy

from lib.utils import get_config
from lib import logger
from datetime import datetime

# The data structure that represent the graph is a nested dictionary composed by three dictionaries:
# ds = {
#      'nodes': {},
#      'relations': {},
#      'adjacency': {}
# } datastruct
#
# All the nodes are uniquely identified by their attribute internal_id, each relation is identified
# by the couple (sid, eid) where sid/eid is the source/end internal id.
#
# nodes:     Is a dict of unique nodes. Each node is a dict identified by the
#            value of the attribute "internal_id".  Each dictionary stores all the
#            attribute of the node: nodes[node_internal_id] = {attrib1:value1, attrib2:value2, ...}
#
# relations: a dict indexed by the tuple (sid, eid) that stores all the
#            attributes of the relation sid <-> eid
#
# adjacency: a dict of the adjacency list representation of the graph,
#            adjacency[sid] stores the list of id of the neighbors of source_id


NPTYPE    = ['evento', 'luogo', 'veicolo', 'utenzabancaria', 'utenzatlc', 'stupefacente', 'documento']
PTYPE     = ['persona', 'organizzazione', 'private']

PERSONE   = ['persona']
ORGANIZZAZIONI = ['organizzazione']

ALLTYPES  = PTYPE + NPTYPE
RELATIONS = ['collegato', 'contatta', 'parente', 'sede', 'residente', 'domicilio', 'recapito', \
             'localizzato', 'nascita', 'cooccorrenza', 'appears_in_document', 'possible_duplicate']
NPATTR_TO_GIVE = {   
                'evento':  ['internal_id', 'tipoevento', 'dataevento', 'indirizzocompleto'], 
                'luogo':   ['internal_id', 'indirizzocompleto', 'latitude', 'longitude'],
                'veicolo': ['internal_id', 'modello', 'numerotarga'],
                'utenzabancaria': ['internal_id', 'tipo'],
                'utenzatlc': ['internal_id', 'numerotelefono'],
                'stupefacente': ['internal_id', 'descrizione'],
                'documento': ['internal_id', 'drereference']
            }

REL_TYPE_VIA_PUBLIC = 'vp'
color_map = ['red', 'blue', 'green', 'yellow', 'orange', 'pink', 'violet']


linkw = dict()

linkw['collegato']              = 1.0
linkw['contatta']               = 1.0
linkw['parente']                = 0.9
linkw['sede']                   = 0.9
linkw['residente']              = 0.5
linkw['domicilio']              = 0.5
linkw['recapito']               = 0.5
linkw['localizzato']            = 0.5
linkw['nascita']                = 0.3
linkw['cooccorrenza']           = 0.05
linkw['appears_in_document']    = 0.01


linkw['vnp_collegato']           = 1.0 * 0.5
linkw['vnp_contatta']            = 1.0 * 0.5
linkw['vnp_parente']             = 0.9 * 0.5
linkw['vnp_sede']                = 0.9 * 0.5
linkw['vnp_residente']           = 0.5 * 0.5
linkw['vnp_domicilio']           = 0.5 * 0.5
linkw['vnp_recapito']            = 0.5 * 0.5
linkw['vnp_localizzato']         = 0.5 * 0.5
linkw['vnp_nascita']             = 0.3 * 0.5
linkw['vnp_cooccorrenza']        = 0.05* 0.5
linkw['vnp_appears_in_document'] = 0.01* 0.5


linkw['possible_duplicate']     = 0.1 
linkw['added_degree']           = 0.01


debug = 1;
edgelist_file = "edges.txt"
vertices_file = "vertices.txt"


class graphDS:
    ""
    ds = {}
    def __init__(self):
        global ds
        self.ds = graphDS.ds
        pass

    def newDS(self):
        myds = {}
        self.setDS(myds, adjacency = {}, relations = {}, nodes = {})
        return myds
    
    def getDS(self):
        if bool(self.ds):
            return self.ds
        else:
            self.setDS(adjacency = {}, relations = {}, nodes = {})
        return self.ds

    def setDS(self, myds=None, adjacency=None, relations=None, nodes=None):
        if myds is None:
            self.ds['adjacency'] = adjacency
            self.ds['relations'] = relations
            self.ds['nodes']     = nodes
        else:
            myds['adjacency'] = adjacency
            myds['relations'] = relations
            myds['nodes']     = nodes
        return True

    def getNodeName(self, i, tag='oldtag'):
        itag = self.ds['nodes'][i][tag]
        if itag in PERSONE:
            iname = self.ds['nodes'][i]['nominativo']
        elif itag in ORGANIZZAZIONI:
            iname = self.ds['nodes'][i]['denominazione']
        else:
            print("WARNING: tag is neither persona nor organizzazione", file = sys.stderr)
            print(self.ds['nodes'][i][tag])
            iname = None

        return iname

    def dumps(self):
        if bool(self.ds):
            with open(edgelist_file, 'w') as f:
                for (i, j), rels in self.ds['relations'].items():
                    iname = self.getNodeName(i)
                    jname = self.getNodeName(j)
                    print(i, j, iname, jname, rels, file = f)

            print("Edge list dumped to {}".format(edgelist_file))

            
            with open(vertices_file, 'w') as f:
                for nodeid, node_attribs in self.ds['nodes'].items():
                    node_name = self.getNodeName(nodeid)
                    node_tag  = self.ds['nodes'][nodeid]['oldtag']
                    print(nodeid, node_name, node_tag, file = f) 

            print("Node list dumped to {}".format(vertices_file))
        
        else:
            print("WARNING dump_edges: Datastruct is null.", file = sys.stderr)
            return False

        return True

class datamanager:

    """A set of modules to query Neo4j using Py2neo, using a local 
       caching mechanism implemented via pickle.
    """
    document = None

    def __init__(self):
        # Connect to Neo4j DB using the string defined in the configuration file
        config            = get_config()
        connection_string = config.get('Neo4j', 'connection_string')
        cachedir          = config.get('Cache', 'directory')
        self.statfile     = config.get('Fout' , 'datamanager_statfile')
        self.docfile      = config.get('Document' , 'document_outfile') 
        self.docstr       = config.get('Document' , 'document_string')
        self.RELATIONS    = RELATIONS
        self.connection   = self.getNeoConnection(connection_string)
        self.cachedir     = cachedir
        self.time         = datetime.now().time().isoformat()
        #self.statfile     = self.statfile + '_' + self.time
        #self.docfile     = self.docfile + '_' + self.docstr + '.out'

    @staticmethod
    def getNeoConnection(connection_string):
        try:
            mygraph = Py2neoGraph(connection_string)
        except:
            print("WARNING datamanager: Error connecting to Neo4j db.", file = sys.stderr)
            mygraph = False
        
        return mygraph
        
    def getSentences(self, usecache=True):
        if datamanager.document is not None:
            return datamanager.document
        else:
            # Query mydocument
            query_document = 'MATCH (n)-[a]-(d:documento) WHERE d.regione=~"' + self.docstr + '" RETURN distinct d'
            cachefile       = self.getCacheName(query_document)
            datamanager.document = self.testCache(cachefile, "mydocument")
            

            # No connection
            if not self.connection:
                print("WARNING: there is no connection, return the result from cache")
                return datamanager.document

            # No cache
            if not datamanager.document or not usecache:
                datamanager.document = self.getSentencesFromGraph(query_document, self.docstr)
                self.saveToCache(datamanager.document, cachefile)
                logger.getLogger().info("Saved {1} mydocument to {0}".format(self.docfile, self.docstr))

            return datamanager.document

    def fillDataStructure(self, query, ds, getSent=True, usecache=True):
        """Fills the datastruct ds associated to the graph obtained
           by issuing the input query to Neo4j.
           If getSent is True also query for documents
        """
        global document
        cachefile = self.getCacheName(query)
        relations = self.testCache(cachefile, "graph data structure")

        # No connection
        if not self.connection:
            print("WARNING: there is no connection, return the result from cache")
            ds['nodes']     = relations['nodes']
            ds['relations'] = relations['relations']
            ds['adjacency'] = relations['adjacency']
            return True

        # No cache
        if not relations or not usecache:
            relations = self.getQueryDataset(query)
            self.saveToCache(relations, cachefile)

        if getSent is True:
            print("fillDataStructure: get documents")
            document = self.getSentences()

        graphDS().setDS(ds, adjacency=relations['adjacency'], relations=relations['relations'], nodes=relations['nodes'])

        return True


    def getHash(self, n):
        """Returns hashlib.sha256"""
        return hashlib.sha256(n.encode('utf-8')).hexdigest()


    def getCacheName(self, query):
        """Takes as input a Neo4j query and return the corresponding cachefile name"""
        return "{0}/{1}.pickle".format(self.cachedir, self.getHash(query))    


    def testCache(self, cachefile, log_message):
        """Takes as input the cache file name returned by getCacheName and
           search for it in the local cached DB.
           Returns the object cached upon success None otherwise.
           Log the operations.
        """
        if os.path.isfile(cachefile):
            with open(cachefile, 'rb') as f:
                myobj = pickle.load(f)
            logger.getLogger().info("Loaded {1} from cachefile ({0})".format(cachefile, log_message))
        else:
            myobj = None
        return myobj

    def saveToCache(self, obj, cachefile):
        """Takes as input the object and the corresponding cachefile name returned by getCacheName and
           dump the object to the local cached DB.
           Returns True upon success.
           Log the operations.
        """
        os.makedirs(self.cachedir, exist_ok=True)
        with open(cachefile, 'wb') as f:
            pickle.dump(obj, f)
        logger.getLogger().info("Saved data to cachefile ({0})".format(cachefile))
        return True

    def getSingleQuery(self, query):
            return self.connection.run(query).data()


    def getQueryDataset(self, query):
        """Execute the query to get the graph to the remote Neo4j using Py2neo.
           Returns the datastruct representing the graph.
           NOTE: this function is intended to be used by fillDataStructure!
        """
        myds = graphDS().newDS()

        logger.getLogger().info("Retrieve relations from DB ")
        relations = self.connection.run(query)
        for record in relations.data():
            for k, v in record.items():
                s = v.start_node()
                e = v.end_node()
                r = v.properties
                s = s.properties
                e = e.properties
                sid = s['internal_id']
                eid = e['internal_id']
                lty = r['tag'].lower()

                #print("{0} -- [{1}] -- {2}".format(sid, lty, eid))

                # Stores all the attributes
                myds['nodes'].setdefault(sid, dict())
                myds['nodes'][sid] = s
                myds['nodes'].setdefault(eid, dict())
                myds['nodes'][eid] = e

                myds['adjacency'].setdefault(sid, [])
                myds['relations'].setdefault((sid, eid), [])

                if eid not in myds['adjacency'][sid]:
                    myds['adjacency'][sid].append(eid)
                
                # WARNING relations are duplicated by the py2neo query so I must check for unicity!!
                if lty not in myds['relations'][(sid, eid)]:
                    myds['relations'][(sid, eid)].append(lty)
                    #print("adding relation ({0},{1}) of type: {2}".format(sid, eid, lty))

        if len(myds['relations']) < 1:
            print("WARNING getQueryDataset: empty data structure get from neo4j", file = sys.stderr)

        return myds


    def getSentencesFromGraph(self, query, match_string, usecache=True):
        """ 
           match_string is the string to match in attribute regione of the node documento
        """
        mydocument = {}

        # Check cache
        relations = self.connection.run(query)
   
        # Fill in the dictionary of mydocument 
        i = 0        
        for record in relations.data():
            for k,v in record.items():
                s = v.start_node()
                s = s.properties
                sid = s['internal_id']
                dre = s['drereference']
                mydocument[i] = {'dre' : dre, 'internal_id' : sid}
                i += 1

        # Query for max, min and num of nodes in each document
        # Check cache only for the first query. I assume that if I have the first then I got them all
        sid = str(mydocument[0]['internal_id'])
        query_count = 'MATCH (n)-[a]-(d:documento) WHERE d.regione="campania" AND d.internal_id=' \
                      + sid + ' RETURN count(distinct(n)), max(n.internal_id), min(n.internal_id)' 
        for k, v in mydocument.items():
            sid  = str(v['internal_id'])
            query_count = 'MATCH (n)-[a]-(d:documento) WHERE d.regione="campania" AND d.internal_id=' + sid + \
                          ' RETURN count(distinct(n)) as num, max(n.internal_id) as max, min(n.internal_id) as min' 
            ret = self.connection.run(query_count).data()[0]
            mydocument[k]['num_nodes'] = ret['num']
            mydocument[k]['max_label'] = ret['max']
            mydocument[k]['min_label'] = ret['min']

        #print(json.dumps(mydocument, indent = 4))

        # Create the output file 
        with open(self.docfile, 'w') as f:
            for i in range(len(mydocument)):
                print("{} {} {} {} {} {}".format(i, color_map[i], mydocument[i]['max_label'], mydocument[i]['min_label'],\
                      mydocument[i]['num_nodes'], str(mydocument[i]['dre']).replace(' ', ' ')), file = f)
        
        return mydocument



    def checkAdjaceny(self, ds):
        if (debug == 0): return True
        print("Checking adjacency list ... ")

        if len(ds['nodes'].keys()) < 1:
            print("Data struct without 'nodes', cannot check")
            return False

        else:
            for k, v in ds['adjacency'].items():
                    if k not in ds['nodes']: 
                        print("KEY Node {0} in adjacency not found in ds['nodes']".format(k), file = sys.stderr)
                        sys.exit()
                    for n in v:
                        if n not in ds['nodes']:
                            print("ADJ Node {0} in adjacency not found in ds['nodes']".format(v), file = sys.stderr)
                            sys.exit()

        print("Done, no errors found")
        return True
    
    def checkRelations(self, ds, isund = False):
        if (debug == 0): return True
        print("Checking relations list ... ")
        for (i, j), rels in ds['relations'].items():

            itype = ds['nodes'][i]['tag']
            jtype = ds['nodes'][j]['tag']

            jnot = 0
            inot = 0

            # We have found the edge (i, j) 
            # j should be in the adjacency of i
            # and i should be a key in ds['adjacency']
            if i in ds['adjacency'].keys():
                if j not in ds['adjacency'][i]:
                    jnot = 1
            else:
                    jnot = 2
           
            if isund is True: 
                if j in ds['adjacency']:
                    if i not in ds['adjacency'][j]:
                        inot = 1
                else:
                        into = 2

            if (inot) and (jnot):
                if (inot == 1):
                    print("Error in relations {0}: node {1}:{3} not found in adjacency of {2}:{4}".format(rels, i, j, itype, jtype), file = sys.stderr)
                if (inot == 2):
                    print("Error in relations {0}: {1}:{2} has not an adjacency list".format(rels, j, jtype), file = sys.stderr)
                if (jnot == 1):
                    print("Error in relations {0}: node {1}:{3} not found in adjacency of {2}:{4}".format(rels, j, i, jtype, itype), file = sys.stderr)
                if (jnot == 2):
                    print("Error in relations {0}: {1}:{2} has not an adjacency list".format(rels, i, itype), file = sys.stderr)
                sys.exit()

        print("Done, no errors found")
        return True
    
    def getDistinctNodes(self, ds, count = 1):
        n = set()
        r = set()
        for (v, w) in ds['relations'].keys():
            r.add(v)
            r.add(w)

        for v, vlist in ds['adjacency'].items():
            n.add(v)
            for w in vlist:
                n.add(w)
        
        if len(n - r) != 0:
            print("Error in getDistinctNodes: len r = {}, len n = {}".format(len(r), len(n))) 
            return None
        
        if count == 1:
            print("Set of Nodes in Data Structure = {}".format(len(n)))
            print("Number of nodes in adjacency: {}".format(len(ds['adjacency'])))
            print("Number of relations: {}".format(len(ds['relations'])))
            return(len(n))

        return n

    def getRelationListFromTuple(self, edge, ds):
        """
            Searches for the tuple (NP, P) in ds['relations'], if exist
            return the list of relations, otherwise return None
        """
        lty  = None
        redge = edge[::-1]

        if edge in ds['relations']:
            lty = ds['relations'][edge]
        elif redge in ds['relations']:
            lty = ds['relations'][redge]
        else:
            print(("\nError in getRelationListFromTuple: neither {0} or {1} is in ds['relations']\n")
                  .format(edge, redge), file = sys.stderr)
            sys.exit()

        return lty

    
    def checkDataStruct(self, myds):
        if (debug == 0): return
        self.checkRelations(myds)
        self.checkAdjaceny(myds)
        n = self.getDistinctNodes(myds)
        if n is None:
            print("ERROR in checkDataStruct: getDistinctNodes", file = sys.stderr)
        return n

    def print_adjacency(self, ds, caller):
        """Take datastruct as input and print to stdout ds['adjacency']"""
        for v, vlist in ds['adjacency'].items():
            print("{0}:{1}:  ".format(v, len(vlist)), end="")
            for w in vlist:
                if (v, w) in ds['relations'].keys():
                    print("{0}:[{1}]".format(w, len(ds['relations'][(v, w)])), end=" ")
                else:
                    print(("Error in {1}: {0} is NOT in ds['relations'] but the nodes "
                           "are in the adjacency\n").format((v, w), caller), file = sys.stderr)
                    sys.exit()
            print()
        return None


    def build_structure_to_inject(self, nodes, adjacency, links):

        allentities = {}
        newlinks    = {}
        idx         = 0

        for link, lt in links.items():
            ids, idt = link
            ids = int(ids)
            idt = int(idt)
            dre1 = nodes[ids]['drereference']
            dre2 = nodes[idt]['drereference']

            if (dre1 != dre2):
                pass 
                #print("WARNING drereference differs for nodes {} -> {}".format(ids, idt), file = sys.stderr)
            
            if (ids not in allentities): 
                tag = nodes[ids]['tag']
                eid = nodes[ids]['internal_id']
                if (tag != 'private'):
                    printf("Oh No! There is a Non Private node in adjacencyacency!")
                    sys.exit(111)
                else:
                    allentities[eid] = {
                        'tag':    'private',
                        'entity': dict(nodes[ids].items())
                    }
           
            if (idt not in allentities): 
                tag = nodes[idt]['tag']
                eid = nodes[idt]['internal_id']
                if (tag !=  'private'):
                    printf("Oh No! There is a Non Private node in adjacencyacency!")
                    sys.exit(111)
                else:
                    allentities[eid] = {
                        'tag':    'private',
                        'entity': dict(nodes[idt].items())
                    }

            for i in range(len(lt)):
                newlinks[idx] = {
                "sourceid":        ids, 
                "targetid":        idt, 
                "linkdescription": lt[i], 
                "drereference":    dre1,
                "weight":          linkw[lt[i]],
                "group":           'campania'
                } 
            
           
            #print(idx, newlinks[idx])
            idx += 1

        return (allentities, newlinks)


class datatransform():

    statfile_write = 0

    def __init__(self):
        self.dm = datamanager()

    def dsInfo(self, ds, undirected=False, outfile=None):
        """
        Takes datastruct as input and print statistics informations on it to file.
        Returns None. 
        """
        uds = deepcopy(ds)
        if not undirected:
            if (debug == 1): print("dsInfo: make the Graph undirected to collect nodes informations")
            uds = self.makeUndirected(ds)

        if len(ds['relations']) < 1:
            print("dsInfo: the input datastructure is Empty. Nothing to do", file = sys.stderr)
            return None


        node_types = {}
        rels_types = {}
        attr_types = {}
        isolated_node_set  = set()
        nptype_node_set    = set()
        ptype_node_set     = set()
        doc_node_set       = set()

        totnodes    = 0
        # Compute the number of nodes for each kind
        for nk, nv in ds['nodes'].items():
            #print("nk={} --  nv={}".format(nk, nv))
            node_types.setdefault(nv['tag'], 0)
            node_types[nv['tag']] += 1
            
            for k,v in nv.items():
                attr_types.setdefault(k, 0)
                attr_types[k] += 1
            if (nv['tag'] in PTYPE):
                ptype_node_set.add(nk)
            else:
                #Set of nodes of kind documento
                if (nv['tag'] == 'documento'):
                    doc_node_set.add(nk)
                else:
                    nptype_node_set.add(nk)
            totnodes += 1
        
        # Number of relations for each kind
        totrels = 0
        for rk, rv in ds['relations'].items():
            for elems in range(len(rv)):
                rels_types.setdefault(rv[elems], 0)
                rels_types[rv[elems]] += 1
                totrels += 1

        isolated_node_set = self.getIsolatedNodes(ds)
        isolated_node_new = 0
        if 'isolated' in attr_types:
            isolated_node_new = attr_types['isolated']

        if debug == 1:
            print("tot nodes:     %d" % totnodes)
            print("tot relations: %d" % totrels)
            print("tot isolated:  %d" % isolated_node_new)
            
        if outfile is None:
            outfile = self.dm.statfile  
    
        self.appendToStatfile(outfile, totnodes, "Total number of Nodes", debug)
        self.appendToStatfile(outfile, totrels, "Total number of Relations", debug)
        self.appendToStatfile(outfile, isolated_node_new, "Total number of Isolated Nodes in GOP", debug)
        self.appendToStatfile(outfile, doc_node_set, "SET OF ALL THE DOCUMENT NODES", debug)
        self.appendToStatfile(outfile, node_types, "NUMBER OF NODES OF EACH KIND", debug)
        self.appendToStatfile(outfile, rels_types, "NUMBER OF RELATIONS OF EACH KIND", debug)
        self.appendToStatfile(outfile, attr_types, "NUMBER OF NODES FOR EACH ATTRIBUTE", debug)
        self.appendToStatfile(outfile, isolated_node_set, "SET OF ISOLATED NODES", debug)
        self.appendToStatfile(outfile, ptype_node_set, "SET OF ALL PRIVATE TYPE NODES", debug)
        self.appendToStatfile(outfile, nptype_node_set, "SET OF ALL NON-PRIVATE TYPE NODES", debug)
        logger.getLogger().info("Saved nodes statistics to {0}".format(outfile))

        # Try to help garbage collector
        uds = {}

        return True

    
    def getIsolatedNodes(self, ds, ntype = 'documento'):
        """ Return nodes linked only with one kind of nodes,
            used to return the set of nodes linked only with
            documento. To make it easy I make the graph undirected """

        #print("\nFind the set of nodes linked only with nodes of type: {}".format(ntype))

        if not isinstance(ntype, str):
            print("WARNING in getIsolatedNodes, ntype must be a list of strings", file = sys.stderr)
            return None

        if self.isUndirected(ds) is False:
            newds = self.makeUndirected(ds)
        else:
            newds = deepcopy(ds)

        if ntype == 'documento':
            documents = self.dm.getSentences()
            ntypeids  = [documents[i]['internal_id'] for i in range(len(documents))]
        else:
            print("WARNING in getIsolatedNodes: ntype must be 'documeto'", file = sys.stderr)
            return None
        
        nodeset = set()
        count   = 0

        for v, vlist in newds['adjacency'].items():
            if len(vlist) < 1:
                print("WARNING, found empty adajcency in {}".format(v))
            if len(vlist) == 1:
                if vlist[0] in ntypeids:
                    nodeset.add(v)
            else:
                count += 1

        #print("Number of nodes in relation with only documents: {}".format(len(nodeset)))
        #print("Nodes of type documento: {}".format(ntypeids))
        #print("Number of nodes with degree > 1 = {}".format(count))

        del newds
    
        return nodeset
    
    
    def getNodeType(self, nid, myds, tag='tag'):
        return myds['nodes'][nid][tag]

    
    def getEdgeList(self, edge, ds):
        """ Searches for the tuple (NP, P) in ds['relations'], if exist
            return the list of relations, otherwise return None """
        elist  = None

        if edge in ds['relations']:
            elist = ds['relations'][edge]
            
        return elist

    
    def SelectSubgraph(self, ds, nodetype, tag='tag', special_nodes=[]):

        print("\nSelectSubgraph: with nodes of type {} from data structure".format(nodetype))
        print("SelectSubgraph: using tag = %s" % tag)
        if len(special_nodes) > 0:
            print("SelectSubgraph: special nodes to add: {}".format(special_nodes))

        newrel = {}
        newadj = {}
        newnod = {}
        added_set = set()
        count_special_added = 0
        special_copy = special_nodes[:]
        # For each node in relations, select those of type nodetype
        # and add them to the new relations  
        for (u, v) in ds['relations'].keys():
            utype = self.getNodeType(u, ds, tag=tag)
            vtype = self.getNodeType(v, ds, tag=tag)
            #print("{}:{} -- {}:{}".format(u, v, utype, vtype))

            if u in special_copy:
                special_copy.remove(u)
                for w in ds['adjacency'][u]:
                    wtype = self.getNodeType(w, ds, tag=tag) 
                    if wtype in nodetype:
                        print("Adding special node {}:{} to {}:{}".format(u, ds['nodes'][u]['nominativo'], w, ds['nodes'][w]['denominazione']))
                        if (w, u) in ds['relations']:
                            lty = self.getEdgeList((w, u), ds)
                        elif (u, w) in ds['relations']:
                            lty = self.getEdgeList((u, w), ds)
                        else:
                            print("SelectSubgraph: Error, neither (u,w) or (w,u) in relations", file = sys.stderr)
                            sys.exit()
                        newrel.setdefault((w, u), [])
                        newrel[(w, u)] += lty
                        count_special_added += 1
                        added_set.add(ds['nodes'][u]['nominativo'])

            if v in special_copy:
                special_copy.remove(v)
                if v in ds['adjacency']:
                    for w in ds['adjacency'][v]:
                        wtype = self.getNodeType(w, ds, tag=tag) 
                        if wtype in nodetype:
                            print("Adding special node {}:{} to {}:{}".format(v, ds['nodes'][v]['nominativo'], w, ds['nodes'][w]['denominazione']))
                            if (w, v) in ds['relations']:
                                lty = self.getEdgeList((w, v), ds)
                            elif (v, w) in ds['relations']:
                                lty = self.getEdgeList((v, w), ds)
                            else:
                                print("SelectSubgraph: Error, neither (v,w) or (w,v) in relations", file = sys.stderr)
                                sys.exit()
                            newrel.setdefault((w, v), [])
                            newrel[(w, v)] += lty
                            count_special_added += 1
                            added_set.add(ds['nodes'][v]['nominativo'])

            if utype in nodetype and vtype in nodetype:
                lty = self.getEdgeList((u, v), ds)
                newrel.setdefault((u, v), [])
                newrel[(u, v)] += lty

        if len(special_nodes) > 0:
            print("SelectSubgraph: added {} special nodes".format(count_special_added))
            print("SelectSubgraph: added set: {}".format(added_set))
                                
                
        for (u, v) in newrel.keys():
            newadj.setdefault(u, [])
            if v not in newadj[u]:
                newadj[u].append(v)

            newnod.setdefault(u, {})
            newnod.setdefault(v, {})
            newnod[u] = ds['nodes'][u]
            newnod[v] = ds['nodes'][v]
        
        newds = graphDS().newDS() 
        newds['adjacency'] = deepcopy(newadj)
        newds['relations'] = deepcopy(newrel)
        newds['nodes']     = deepcopy(newnod)

        if debug == 1:
            for (u, v) in newds['relations'].keys():
                if u not in newds['adjacency']:
                    print("ERROR in SelectSubgraph, u not in newds['adjacency']", file = sys.stderr)
                if (u, v) not in newds['relations'].keys():
                    print("ERROR in SelectSubgraph, (u, v) not in newds['relations']", file = sys.stderr)
                if u not in newds['nodes']:
                    print("ERROR in SelectSubgraph, u not in newds['nodes']", file = sys.stderr)

        return newds
    
    def SelectSubgraph2Type(self, ds, nodetype1, nodetype2, undirected = True):
        """Build a sub graph with all type2 nodes as keys in ds['adjacency']
           and type2 nodes as neighbors. 
           If undirected is True, consider the subset of relations type1 --> type2 
           as undirected and process both (u, v) and (v, u) if u or v is of type1. 
           For example:
                (NP1, P2) (NP1, P3) (P1, P2) (P1, NP3)
                become:
                (NP1, P2) (NP1, P3) (NP3, P1)

           if undirected is False the above example become:
                (NP1, P2) (NP1, P3)

           The resulting graph however is directed, all relations are type1 --> type2. 
        """

        print("\nSelect subgraph with {} --> {} relations".format(nodetype1, nodetype2))


        self.dm.checkDataStruct(ds)

        newrel = {}
        newadj = {}
        newnod = {}
        for (u, v) in ds['relations'].keys():
            utype = self.getNodeType(u, ds)
            vtype = self.getNodeType(v, ds)
            if utype in nodetype1 and vtype in nodetype2:
                lty = self.getEdgeList((u, v), ds)
                newrel.setdefault((u, v), [])
                newrel[(u, v)] += lty
            # If the undirected Flag is True process also the inverse edge
            # If the inverse is already in relations we will encounter and
            # process it in the above loop
            if undirected is True and (v, u) not in ds['relations'].keys():
                if utype in nodetype2 and vtype in nodetype1:
                    # Use the link type of the straight edge
                    lty = self.getEdgeList((u ,v), ds)
                    newrel.setdefault((v, u), [])
                    newrel[(v, u)] += lty
                
        for (u, v) in newrel.keys():
            newadj.setdefault(u, [])
            if v not in newadj[u]:
                newadj[u].append(v)

            newnod.setdefault(u, {})
            newnod.setdefault(v, {})
            newnod[u] = ds['nodes'][u]
            newnod[v] = ds['nodes'][v]
        
        newds = graphDS().getDS() 
        newds['adjacency'] = deepcopy(newadj)
        newds['relations'] = deepcopy(newrel)
        newds['nodes']     = deepcopy(newnod)
        
        if debug == 1:
            for (u, v) in newds['relations'].keys():
                if u not in newds['adjacency']:
                    print("ERROR in SelectSubgraphType2, u not in newds['adjacency']", file = sys.stderr)
                if (u, v) not in newds['relations'].keys():
                    print("ERROR in SelectSubgraphType2, (u, v) not in newds['relations']", file = sys.stderr)
                if u not in newds['nodes']:
                    print("ERROR in SelectSubgraphType2, u not in newds['nodes']", file = sys.stderr)

        return newds

    
    def attachNodes(self, ds, nodes_to_add, selected_nodes, node_attribs, method):
        """ Add nodes in the list nodes_to_add to the input data structure ds.
            Each node is added to the adjacency list of one node in ds selcted
            from a set build. The set of selected nodes is constructed depending
            of the choosen method (for example maximum degree set).
            node_attribs is the ds['nodes'] dictionary with all the attributes of
            the node
        """

        input_error = 0
        if not isinstance(selected_nodes, list): input_error += 1
        if not isinstance(node_attribs, dict): input_error +=10
        if not isinstance(method, str): input_error +=100
        if input_error > 0:
            print("Error: {} input object are wrong! Abort".format(input_error), file = sys.stderr)
            sys.exit() 

        newds      = deepcopy(ds)
        num_to_add = len(nodes_to_add)
        num_select = len(selected_nodes)

        print("\nAttaching {} input nodes to {} ds nodes using {} method".format(num_to_add, num_select, method))

        for v in nodes_to_add:
            if v in ds['adjacency']:
                print("Error in attachNodes: node {} already in adjacency. Abort".format(v), file = sys.stderr)
                sys.exit()
            if v in ds['nodes']:
                print("Error in attachNodes: node {} already in ds['nodes'].".format(v), file = sys.stderr)
                sys.exit()
            if v not in node_attribs:
                print("Error in attachNodes: node {} not in node_attribs.".format(v), file = sys.stderr)
                sys.exit()
        
        if (method == 'degree'):
            newadj = {}
            newrel = {}
            newnod = {}
            count  = 0

            for v in nodes_to_add:
                u   = random.choice(selected_nodes)[0]
                lty = 'added_degree'
                newds['relations'].setdefault((u, v), []) 
                newds['relations'][(u, v)] += [lty]
                count += 1
                if u not in newds['nodes']:
                    print("Error in attachNodes: node {} not in ds['nodes']. Abort".format(u), file = sys.stderr)
                    sys.exit()
                if u not in newds['adjacency']:
                    print("Error in attachNodes: node {} not in ds['adjacency']. Abort".format(u), file = sys.stderr)
                    sys.exit()

            for (u, v) in newds['relations'].keys():
                if v not in newds['adjacency'][u]:
                    newds['adjacency'][u].append(v)
                    newds['nodes'].setdefault(v, {})
                    newds['nodes'][v] = deepcopy(node_attribs[v])

        else:
            printf("WARNING method {0} Not implented.")
            return None

        return newds
    
    def CliquifizeNode(self, nodeid, nodelist):
        """
            nodelabel is a string/int
            nodelist is its adjacency list with string/int of its neighbors: [node1, node2, ...]
            retrun a dictionary of all the adjacency lists:
            {node1: [list1], node2: [lis2], ...}
        """

        d = {}
        for n in nodelist:
            #print("Cliquifizing node {}".format(nodelabel))
            nlist = nodelist[:]
            d.setdefault(n, nlist)
            d[n].remove(n)
            # DON'T REMOVE ZERO LENGTH LISTS!! THEY ARE USED BY THE CALLING FUNCTION
            # if len(d[n]) == 0:
            #    d.pop(n)
        
        return d

    def makeNodeAttribToGive(self, n, nodetype, myds_nodes):
        d = {}
        for attrib in NPATTR_TO_GIVE[nodetype]:
            if attrib in myds_nodes[n]:
                d[attrib] = myds_nodes[n][attrib]
            else:
                d[attrib] = ' '
        return d

    def extendNodeAttrib(self, n, label, attrib_dict, myds_nodes):
        for k, value in attrib_dict.items():
            klab = label + '_' + k
            myds_nodes[n].setdefault(klab, [])
            myds_nodes[n][klab] = attrib_dict[k]
        return True
    
    def CliquifizeGraph(self, ds, nodetype, toremove = None):

        if toremove is None:
            toremove = set()

        cc = 0
        
        isolated_nodes_neighbors = set()
        isolated_nodes_nodetypes = set()
        newadj = deepcopy(ds['adjacency'])
        newrel = deepcopy(ds['relations'])
        newnod = deepcopy(ds['nodes']) 

        for v, vlist in ds['adjacency'].items():
            vtype = self.getNodeType(v, ds)
            if vtype in nodetype:
                if len(vlist) == 1:
                    isolated_nodes_nodetypes.add(v)
                    newadj.pop(v)
                    newrel.pop((v, vlist[0]))
                    continue
                elif len(vlist) == 0:
                    print("ERROR in CliquifizeGraph: node {} has empty adjacency list. Abort", file = sys.stderr)
                    sys.exit()
                else:
                    cc += 1
                # Remove myself from adjacency
                newadj.pop(v)
                # Stores attributes of v that will pass to its neighbors
                attributes_to_give = self.makeNodeAttribToGive(v, vtype, newnod)

                # Cliquifize my list of neighbors
                clique_dict = self.CliquifizeNode(v, vlist)

                # Add every node to adjacency 
                for w, wlist in clique_dict.items():

                    # Stores all relations and remove them from the list
                    lty = ['vnp_' + x for x in newrel[(v, w)]]
                    newrel.pop((v, w))
                    if len(wlist) < 1:
                        # Track lost nodes 
                        isolated_nodes_neighbors.add(w)
                    else:
                        newadj.setdefault(w, [])
                        label = str(vtype) + '_' + str(v)
                        self.extendNodeAttrib(w, label, attributes_to_give, newnod)
                        # This is tricky because the same w node can be found
                        # in several clique_dict (is connected to several nodetype
                        # nodes)
                        for z in wlist:
                            if z not in newadj[w]:
                                newadj[w].append(z)
                                newrel.setdefault((w, z), [])
                                newrel[(w, z)] += lty

                        # NO NEED TO MODIFY ds['nodes']
                        # All w and z are already in ds['nodes'] because they
                        # where in adjacency
                        
                if (debug == 1):
                    lenadj = len(vlist)
                    nn = lenadj * (lenadj - 1)
                    lencli = sum([len(c) for c in clique_dict.values()])
                    assert nn == lencli
                    #if nn > 0:
                    #    print("------------------------------------")
                    #    print("V = {}:{}".format(v, vtype))
                    #    print("isolated_nodes_nodetypes: {}".format(isolated_nodes_nodetypes))
                    #    print("isolated_nodes_neighbors: {}".format(isolated_nodes_neighbors))
                    #    print("ADJACENCY:{} {}".format(lenadj, ds['adjacency'][v]))
                    #    print("CLIQUE:{} {}".format(lencli,clique_dict))
                    #    print("N = {}".format(nn))
                    #    print("------------------------------------")


        print("\nCliquifized {} nodes of type {}".format(cc, nodetype))
        print("Num isolated nodetypes: {}".format(len(isolated_nodes_nodetypes)))
        print("Num isolated neighbors: {}".format(len(isolated_nodes_neighbors)))
        newds = graphDS().getDS() 
        newds['adjacency'] = deepcopy(newadj)
        newds['relations'] = deepcopy(newrel)
        newds['nodes']     = deepcopy(newnod)
        
        if debug == 1:
            for (u, v) in newds['relations'].keys():
                if u not in newds['adjacency']:
                    print("ERROR in CliquifizeGraph, u not in newds['adjacency']", file = sys.stderr)
                if (u, v) not in newds['relations'].keys():
                    print("ERROR in CliquifizeGraph, (u, v) not in newds['relations']", file = sys.stderr)
                if u not in newds['nodes']:
                    print("ERROR in CliquifizeGraph, u not in newds['nodes']", file = sys.stderr)


                for v, vlist in newds['adjacency'].items():
                    vtype = newds['nodes'][v]['tag']
                    if vtype in nodetype:
                        print("ERROR in CliquifizeGraph: {} is of type {}".format(v, vtype))
                        sys.exit()

                for (u, v) in newds['relations'].keys():
                    vtype = newds['nodes'][v]['tag']
                    utype = newds['nodes'][u]['tag']
                    if vtype in NPTYPE or utype in NPTYPE:
                        print("NTYPE in relation ({}:{}, {}:{})".format(u, utype, v, vtype))
                        sys.exit()

        return newds

    def mergeGraph(self, ds1, ds2):
        """
            Merge the adjacency lists of the two datastruct.
            Remove duplicates in adjacency.
            Add all the relations.
            Add both (u, v) and (v, u)
        """

        print("\nMerge graph to the rescue!")

        newds = deepcopy(ds1)

        # Add all relations from ds2 to ds1 (==> newds)
        for r2, lty2 in ds2['relations'].items():
            newds['relations'].setdefault(r2, [])
            newds['relations'][r2] += lty2

        # Loop over relations
        for (u, v) in newds['relations'].keys():
            if u not in newds['adjacency']:
                newds['adjacency'][u] = [] 
                newds['nodes'][u] = ds2['nodes'][u]
            if v not in newds['adjacency'][u]:
                newds['adjacency'][u].append(v)
                newds['nodes'][u] = ds2['nodes'][u]
        
        if debug == 1:
            for (u, v) in newds['relations'].keys():
                if u not in newds['adjacency']:
                    print("ERROR in CliquifizeGraph, u not in newds['adjacency']", file = sys.stderr)
                if (u, v) not in newds['relations'].keys():
                    print("ERROR in CliquifizeGraph, (u, v) not in newds['relations']", file = sys.stderr)
                if u not in newds['nodes']:
                    print("ERROR in CliquifizeGraph, u not in newds['nodes']", file = sys.stderr)


                for v, vlist in newds['adjacency'].items():
                    vtype = newds['nodes'][v]['tag']
                    if vtype in NPTYPE:
                        print("ERROR in CliquifizeGraph: {} is of type {}".format(v, vtype))
                        sys.exit()

                for (u, v) in newds['relations'].keys():
                    vtype = newds['nodes'][v]['tag']
                    utype = newds['nodes'][u]['tag']
                    if vtype in NPTYPE or utype in NPTYPE:
                        print("NTYPE in relation ({}:{}, {}:{})".format(u, utype, v, vtype))
                        sys.exit()
            
        return newds


    def buildGraphOfPrivateNodes(self, ds, with_isolated = True):

        print("\nStarting Transformation ---> to Graph of Private")

        # Build a list of id od nodes of type documento
        documents = self.dm.getSentences()
        docids    = [documents[i]['internal_id'] for i in range(len(documents))]

        gop_ds = None
        
        print("Original Graph:")
        self.dm.checkDataStruct(ds)

        if with_isolated is False:
            # Remove central nodes documento from datastructure
            nodetype = ALLTYPES
            nodetype.remove('documento') 
            newds = self.SelectSubgraph(ds, nodetype)
            self.dm.checkDataStruct(newds)

        else:
            # Before: get isolated nodes and attach them to the graph ds
            isolated_nodes = self.getIsolatedNodes(ds)

            # Remove central nodes documento from datastructure
            # This will also remove all isolated nodes
            nodetype = ALLTYPES
            nodetype.remove('documento') 
            newds = self.SelectSubgraph(ds, nodetype)
            self.dm.checkDataStruct(newds)
            if(self.findNodeType(newds, 'documento')):
                print("Error found node of type documento in newds", file = sys.stderr)
                sys.exit()

            # Attach isolated nodes to nodes with maximum degree
            numnodes = int(len(ds['nodes'])/100 + 1)
            selected_nodes = self.getMaxDegreesNodes(newds, numnodes) # return (node_id, degree)
            selected_nodes_attrib = {}
            for n in isolated_nodes:
                selected_nodes_attrib.setdefault(n, ds['nodes'][n])
            newds = self.attachNodes(newds, isolated_nodes, selected_nodes, selected_nodes_attrib, 'degree')
            self.dm.checkDataStruct(newds)
            for node in isolated_nodes:
                newds['nodes'][node]['isolated'] = True


        # First: build a ds with all nodes of type PTYPE
        print("\nBuilding initial Graph Private --> Private")
        gop_ds = self.SelectSubgraph(newds, PTYPE)
        self.dm.checkDataStruct(gop_ds)
        if(self.findNodeType(gop_ds, NPTYPE)):
           print("Error node of type NPTYPE found initial GOP", file = sys.stderr)
           sys.exit()


        # Second: build a ds NP->P and Cliquifize NP nodes
        print("\nBuilding Graph Non Private --> Private")
        gonp_ds = self.SelectSubgraph2Type(newds, NPTYPE, PTYPE)
        self.dm.checkDataStruct(gonp_ds)
        for v, vlist in gonp_ds['adjacency'].items():
            vtype = gonp_ds['nodes'][v]['tag']
            if vtype in PTYPE:
                print("Error node of type PTYPE found in GONP", file = sys.stderr)
                sys.exit()
            for w in vlist:
                wtype = gonp_ds['nodes'][w]['tag']
                if wtype in NPTYPE:
                    print("Error node {} of type NPTYPE found in the adjacency list of {} in GONP"\
                          .format((w, wtype), (v, vtype)), file = sys.stderr)
                    sys.exit()

        gonp_ds = self.CliquifizeGraph(gonp_ds, NPTYPE, docids) 
        self.dm.checkDataStruct(gonp_ds)
        if(self.findNodeType(gonp_ds, NPTYPE)):
            print("Error node of type NPTYPE found in cliquifized GONP", file = sys.stderr)
            sys.exit()

        # Third merge the data struct get in the first step with the one in the second
        print("\nBuilding final Graph Of Private Nodes")
        gop_ds = self.mergeGraph(gop_ds, gonp_ds)
        self.dm.checkDataStruct(gop_ds)

        for u, ulist in gop_ds['adjacency'].items():
            gop_ds['nodes'][u]['oldtag'] =  self.getNodeType(u, newds)
            gop_ds['nodes'][u]['tag']    = 'private'
            for v in ulist:
                gop_ds['nodes'][v]['oldtag'] = self.getNodeType(v, newds)
                gop_ds['nodes'][v]['tag']    = 'private'

        return gop_ds
    
    def findNodeType(self, ds, ntype):

        for v, vlist in ds['adjacency'].items():
            vtype = ds['nodes'][v]['tag']
            if vtype in ntype:
                return True
            for w in vlist:
                wtype = ds['nodes'][v]['tag']
                if wtype in ntype:
                    return True

        for (u, v) in ds['relations'].keys():
            utype = ds['nodes'][u]['tag']
            vtype = ds['nodes'][v]['tag']
            if utype in ntype or vtype in ntype:
                return True
    
        return False
    
    def isUndirected(self, ds):
        """
        """
        nonzero = False

        # Check adjacency
        for k, vlist in ds['adjacency'].items():
            # If all adjacecny list are empty return None
            # The line belows test if adjaecncy of node k is empt (usa' len non ce piaceva)
            if(bool(ds['adjacency'].get(k))): 
                nonzero = None
            for w in vlist:
                if w in ds['adjacency'] and k not in ds['adjacency'][w]:
                    return False

        # Check relations
        for (u, v) in ds['relations'].keys():
            if(bool(ds['adjacency'].get((u, v)))): 
                nonzero = None
            if (v, u) not in ds['relations'].keys():
                return False

        return nonzero

    def makeUndirected(self, ds):
        """ Takes the datastruct as Input and make the adjacency list symmetric thus
            the graph undirected. Retruns a new datastruct with a symmetric adjacency list.
        """

        uds = deepcopy(ds)

        for v, vlist in ds['adjacency'].items():
            for w in vlist:
                if w in ds['adjacency']:
                    if v not in uds['adjacency'][w]:
                        uds['adjacency'][w].append(v)
                        lty = self.dm.getRelationListFromTuple((v, w), ds)
                        uds['relations'].setdefault((w, v), [])
                        uds['relations'][(w, v)] += lty
                else:
                    uds['adjacency'].setdefault(w, [])
                    uds['adjacency'][w].append(v)
                    lty = self.dm.getRelationListFromTuple((v, w), ds)
                    uds['relations'].setdefault((w, v), [])
                    uds['relations'][(w, v)] += lty

        return uds
    
    def getMaxDegreesNodes(self, ds, numnodes):
        """Return a list of n tuple (noded_id, degree) sorted
           by degree """
        sorted_ds = sorted(ds['adjacency'].items(), key=lambda x: len(x[1]), reverse = True)
        nmax = [] 
        for i in range(numnodes):
            nmax.append((sorted_ds[i][0], len(sorted_ds[i][1])))
        return nmax


    def appendToStatfile(self, filename, obj, header, debug):
        if (debug == 0):
            return

        if (self.statfile_write == 0):
            self.statfile_write = 1
            with open(filename, 'w') as f:
                f.write("---------- {0} ----------\n".format(self.dm.time))

        iterable = 0
        notiterable = 0
        try:
            iterator = iter(obj)
        except TypeError:
            notiterable = 1
        else:
            iterable    = 1

        if filename == 'stdout':
            if (iterable == 1):
                    print(header, file = sys.stdout)
                    print("Number of elements: {0}".format(len(obj)), file = sys.stdout)
                    if isinstance(obj, set):
                        obj=list(obj)
                    print(json.dumps(obj, indent=4), file = sys.stdout)
                    print("", file = sys.stdout)
            else:
                print("{0}: {1}".format(header, obj), file = sys.stdout)
        else:
            with open(filename, 'a') as f:
                if (iterable == 1):
                        print(header, file = f)
                        print("Number of elements: {0}".format(len(obj)), file = f)
                        if isinstance(obj, set):
                            obj=list(obj)
                        print(json.dumps(obj, indent=4), file = f)
                        print("", file = f)
                else:
                    print("{0}: {1}".format(header, obj), file = f)

        return True

