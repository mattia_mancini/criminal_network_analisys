from igraph import *
from itertools import combinations as comb
from itertools import tee
from tabulate import tabulate
from random import randint
import math
from lib import logger
from datetime import datetime

from lib.datamanager import datamanager
from lib.datamanager import linkw as LINKW
from lib.datamanager import ORGANIZZAZIONI
from lib.utils import get_config
from collections import OrderedDict as odict

default_label = {'persona': ['nominativo'], 'organizzazione': ['denominazione', 'nominativo'], \
                 'evento': ['tipoevento', 'dataevento'], 'luogo': ['indirizzocompleto'], \
                 'veicolo': ['tipoveicolo'], 'utenzabancaria': ['tipo'], 'utenzatlc': ['tiponumero'],\
                 'stupefacente': ['descrizione'], 'documento': ['drereference'], 'individuo':['nominativo'],\
                 'gruppo':['nominativo']}

color_map  = ['yellow', 'orange', 'blue', 'red', 'green', 'pink', 'violet']
color_map1 = ['orangered2', 'darkolivegreen3', 'light steel blue', 'violet red', 'mediumpurple', 'goldenrod3', 'yellow4' ]

default_widthmap = [0.1, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0]
colormap_grey    = ['#CCCCCC', '#808080', '#707070', '#606060', '#505050', '#404040', '#303030', '#202020', '#101010', '#000000']
special_color    = 'darkorange3'

default_shape    = {
                    'special'   : {'size':90, 'shape':'circle', 'label_size':60},
                    'special2'  : {'size':60, 'shape':'circle', 'label_size':40},
                    'above'     : {'size':40, 'shape':'circle', 'label_size':30},
                    'mean'      : {'size':30, 'shape':'circle', 'label_size':20},
                    'below'     : {'size':20, 'shape':'circle', 'label_size':10},
                    'min'       : {'size':10,  'shape':'circle', 'label_size':0},
                    'zero'      : {'size':100, 'shape': 'square', 'label_size':30}
                 }
paths_shape    = {
                    'special'   : {'size':70, 'shape':'circle', 'label_size':40},
                    'special2'  : {'size':40, 'shape':'circle', 'label_size':20},
                    'above'     : {'size':20, 'shape':'circle', 'label_size':20},
                    'mean'      : {'size':20, 'shape':'circle', 'label_size':20},
                    'below'     : {'size':20, 'shape':'circle', 'label_size':10},
                    'min'       : {'size':10,  'shape':'circle', 'label_size':0},
                    'zero'      : {'size':100, 'shape': 'square', 'label_size':30}
                 }
large_shape    = {
                    'special'   : {'size':120, 'shape':'circle', 'label_size':60},
                    'special2'  : {'size':100, 'shape':'circle', 'label_size':40},
                    'above'     : {'size':90, 'shape':'circle', 'label_size':40},
                    'mean'      : {'size':60, 'shape':'circle', 'label_size':30},
                    'below'     : {'size':30, 'shape':'circle', 'label_size':20},
                    'min'       : {'size':20,  'shape':'circle', 'label_size':10},
                    'zero'      : {'size':100, 'shape': 'square', 'label_size':30}
                 }

shape_document    = {
                    'special'   : {'size':0, 'shape':'circle', 'label_size':0},
                    'special2'  : {'size':0, 'shape':'circle', 'label_size':0},
                    'above'     : {'size':60, 'shape':'circle', 'label_size':40},
                    'mean'      : {'size':40, 'shape':'circle', 'label_size':30},
                    'below'     : {'size':20,  'shape':'circle', 'label_size':5},
                    'min'       : {'size':10,  'shape':'circle', 'label_size':0},
                    'zero'      : {'size':100, 'shape': 'square', 'label_size':30}
                 }


supported_file_types = { 'DIMACS':'dimacs edge list', 'EDGE_LIST': 'plain list of edges', 'DL': 'mmm'}

class graphmanager(Graph):

    def __init__(self):
        config         = get_config()
        self.statfile  = config.get('Fout' , 'graphmanager_statfile')
        self.time      = datetime.now().time().isoformat()
        #self.statfile  = self.statfile + '_' + self.time
        self.document  = datamanager().getSentences()
        self.dredict   = self.getDreDict(self.document)

    @staticmethod
    def get_header(fn, ft):
        if ft not in supported_file_types:
            print("ERROR get_header: file type {} not supported".format(ft), file = sys.stderr)
            sys.exit(111)

        header = {}

        if ft == 'DIMACS':
            with open(fn, 'r') as f:
                # Read first line, must have at least 4 fields
                firstline = f.readline()
                nfirst    = len(firstline.split())
                if (nfirst < 4):
                    print('File format error first line has', nfirst, 'fields, should be at least 4! Exit')
                    sys.exit(111)

                while(firstline.split()[0] == 'c'):
                    firstline = f.readline()

                if (firstline.split()[0] != 'p'):
                    print("first character of first line must be p! Exit")
                    sys.exit(111)

                nv   = int(firstline.split()[1])
                ne   = int(firstline.split()[2])
                root = int(firstline.split()[3])

                print("\nReading graph in DIMACS format from file:", fn)
                print("nv =", nv, "ne =", ne, "root =", root)
                print(40 * '-')

                header['nv']   = nv
                header['ne']   = ne
                header['root'] = root
                header['skip'] = 1

            f.close()

        return header

        
    def getGraphFromFile(self, file_name, file_type, weight=False, linkname=False, stag=False, slabel=False, use_tag_as_color=False):
        """
            Read the graph from file. Supported file types must be stored in:
                supported_file_types = { 'DIMACS':'dimacs edge list', 'EDGE_LIST': 'plain list of edges', 'DL': 'mmm'}
            
            When the file is a list of edges in the form (for example DIMACS or EDGE_LIST):
                source, target
                then optional parameters are supported as extra columns. For each parameter an extra column 
                is read in this order: weight, linkname, stag and ttag, slabel and tlabel.
                If you give linkname and slabel then the functions read 5 columns:
                    source, target, linkname, slabel, tlabel
                If stag is given then 2 columns with stag and ttag are read (the same for slabel)
            
            Optionally different color can be assigned to different tags by setting use_tag_as_color as True.
        """

        g = Graph(directed=True)
        w = 0
        stripped_file = None

        with open(file_name, 'r') as f:

            # Depending on file types parse the header 
            header = self.get_header(file_name, file_type)

            ln = 0
            for line in f:
                # Reset attributes count
                i = 0
                if file_type == 'DIMACS':
                    if ln < int(header['skip']):
                        print("skipping header line: {}".format(line))
                        ln += 1
                        continue
                        
                    sid = int(line.split()[1])
                    tid = int(line.split()[2])
                    i = 3
                elif file_type == 'DL':
                    sid = int(line.split()[0][1:])
                    tid = int(line.split()[1][1:])
                    i = 2
                elif file_type == 'EDGE_LIST':
                    sid = int(line.split()[0])
                    tid = int(line.split()[1])
                    i = 2
                ln += 1

                #print("Added relation: {}".format((sid, tid)))
                # Check for attributes
                if weight:
                    w = line.split()[i][:]
                    i += 1
                if linkname:
                    linkname = line.split()[i][:]
                    i += 1
                else:
                    linkname = ''
                if stag:
                    stag = line.split()[i][:]
                    ttag = line.split()[i+1][:]
                    i += 2
                if slabel:
                    slabel = line.split()[i][:]
                    tlabel = line.split()[i+1][:]
                    i += 2
        
                u = self.find_node(str(sid), g) 
                v = self.find_node(str(tid), g) 

                # add_vertex append it to g.vs (according to Tamas) and set the attribute name
                # g.add_vertex(str(sid)) ==> g.vs[id]['name'] = str(sid)
                if not (u):
                    g.add_vertex(str(sid))
                if not (v):
                    g.add_vertex(str(tid))
                   
                # This is the value of the name attribute set above by the function add_vertex 
                uname = str(sid)
                vname = str(tid)

                # The correct index is set below, assuming that the last vertex added is appended 
                # at the end this is confirmed on stackoverflow by Tamas.
                if (not u) and (not v):
                    uid = len(g.vs)-2
                    vid = len(g.vs)-1
                if (not u) and (v):
                    uid = len(g.vs)-1 
                    vid = v.index
                if (u) and (not v):
                    uid = u.index
                    vid = len(g.vs)-1
                if (u) and (v):
                    uid = u.index
                    vid = v.index

                if bool(stag) and bool(ttag):
                    g.vs[uid]['tag'] = stag
                    g.vs[vid]['tag'] = ttag

                if bool(slabel) and bool(tlabel):
                    g.vs[uid]['label'] = slabel 
                    g.vs[vid]['label'] = tlabel
                else:
                    g.vs[uid]['label'] = uname
                    g.vs[vid]['label'] = vname

   
                if use_tag_as_color:
                    while len(set(g.vs['tag']) > len(color_map1)):
                        color_map1 += color_map1
                    g.vs[uid]['color'] = color_map1[g.vs[uid]['docid']]
                    g.vs[vid]['color'] = color_map1[g.vs[vid]['docid']]
           

                #if 'label' in g.vs.attributes():
                #    print("VERTEX U: {} {}".format(g.vs[uid]['name'], g.vs[uid]['label']))
                #    print("VERTEX V: {} {}".format(g.vs[vid]['name'], g.vs[vid]['label']))
                #print("NODE   S: {} {}".format(sid, slabel))
                #print("NODE   E: {} {}".format(tid, tlabel))
          
                g.add_edge(uid, vid, weight = float(w), label = linkname, color = 'darkgrey', width = 0.8)
                
                lid = g.get_eid(uid, vid)
                #print("Adding edge: ({0}, {1}) with attributes: {2} {3}, edge: {4} {5} {6}"\
                #.format(uid, vid, g.vs[uid]['name'], g.vs[vid]['name'], g.es[lid].source, g.es[lid].target, g.es[lid]['label']))       

        print("Create an igraph Graph istance with {} vertices and {} edges".format(g.vcount(), g.ecount()))

        return g



    # I need the tag to set in g the tag that specify the type of the vertex
    # All nodes in ds must have the specified tag!
    def getGraphFromDS(self, ds, nodetag):
        """Create and return an istance of igraph Graph() from ds['relations'].
           Vertices are relabelled to be continuos set of integers, the old index 
           old index:  g.vs[id]['name'] = str(sid)
           tag:        g.vs[sid]['tag'] = s[stag]
           name:       g.vs[sid]['label'] = s[default_label[s[stag]][0]]
           document:   g.vs[sid]['docid'] = self.dredict[s['drereference']]
           Most of the times nodetag should be set as 'oldtag' which refers to 
           the tag in the original data.
        """

        if len(ds['nodes']) < 1:
            print("WARNING: getGraph: ds['nodes'] is empty cannot create the graph!")
            return None
        
        #print('getGraph:')
        #print(len(ds['relations']))
        #print(len(ds['adjacency']))
        #print(len(ds['nodes']))

        g = Graph()

        for (sid, eid), ltylist in ds['relations'].items():
            #print((sid,eid), ltylist)
            s = ds['nodes'][sid]
            e = ds['nodes'][eid]

            stag = etag = nodetag
            if nodetag not in s:
                print("WARNING getGraph: input TAG {} not found, default to 'tag'", sys.stderr)
                stag = 'tag'
            if nodetag not in e:
                print("WARNING getGraph: input TAG {} not found, default to 'tag'", sys.stderr)
                etag = 'tag'

            # Set the label: the value corresponding of the tag
            slabel = s[default_label[s[stag]][0]]
            elabel = e[default_label[e[etag]][0]]

            # Set the id of the document
            sdocid = self.dredict[s['drereference']]
            edocid = self.dredict[e['drereference']]
          
            u = self.find_node(str(sid), g) 
            v = self.find_node(str(eid), g) 

            # add_vertex append it to g.vs (according to Tamas) and set the attribute name
            # g.add_vertex(str(sid)) ==> g.vs[id]['name'] = str(sid)
            if not (u):
                g.add_vertex(str(sid))
            if not (v):
                g.add_vertex(str(eid))
               
            # This is the value of the name attribute set above by the function add_vertex 
            uname = str(sid)
            vname = str(eid)

            # The correct index is set below, assuming that the last vertex added is appended 
            # at the end this is confirmed on stackoverflow by Tamas.
            if (not u) and (not v):
                uid = len(g.vs)-2
                vid = len(g.vs)-1
            if (not u) and (v):
                uid = len(g.vs)-1 
                vid = v.index
            if (u) and (not v):
                uid = u.index
                vid = len(g.vs)-1
            if (u) and (v):
                uid = u.index
                vid = v.index

            

            g.vs[uid]['tag'] = s[stag]
            g.vs[vid]['tag'] = e[etag]

            g.vs[uid]['label'] = slabel 
            g.vs[vid]['label'] = elabel

            # Add docid --> document id (sentenza) attribute
            g.vs[uid]['docid'] = sdocid
            g.vs[vid]['docid'] = edocid
    
            # Add color based on docid using a custom map
            g.vs[uid]['color'] = color_map1[g.vs[uid]['docid']]
            g.vs[vid]['color'] = color_map1[g.vs[vid]['docid']]
           

            #print(uname, vname)
            #print(uid,   vid)
            #print(g.vs[uid])
            #print(g.vs[vid])
            #print("VERTEX U: {} {}".format(g.vs[uid]['name'], g.vs[uid]['label']))
            #print("NODE   S: {} {}".format(sid, slabel))
            #print("VERTEX V: {} {}".format(g.vs[vid]['name'], g.vs[vid]['label']))
            #print("NODE   E: {} {}".format(eid, elabel))
           
            for lty in ltylist:
                lty = lty.lower()
                #print("({0}) -- [({1})] -- ({2})".format(sid, lty, eid))
                if (lty   == 'cooccorrenza'):
                    newlty = 'cooc' 
                elif (lty == 'vnp_cooccorrenza'):
                    newlty = 'vnp_cooc' 
                elif (lty == 'possible_duplicate'):
                    newlty = 'pd'
                else:
                    newlty = lty
                g.add_edge(uid, vid, weight = float(LINKW[lty]), label = newlty, color = 'darkgrey', width = 0.8)
                

            #lid = g.get_eid(uid, vid)
            #print("Adding edge: ({0}, {1}) with attributes: {2} {3}, edge: {4} {5} {6}"\
            #.format(uid, vid, g.vs[uid]['name'], g.vs[vid]['name'], g.es[lid].source, g.es[lid].target, g.es[lid]['label']))       
        return g

    def find_node(self, name, g):
        if not isinstance(name, str):
            print("Error node name not a string.", file=sys.stderr)
            return sys.exit(111)
        else:
            try:
                v = g.vs.find(name)
                return v
            except Exception:
                return None
    

    @staticmethod
    def getDreDict(sent_dict):
        dredict = {}
        for i in range(len(sent_dict)):
            dre = sent_dict[i]['dre']
            dredict[dre] = i
        return dredict


    # Just a wrapper
    def addVertices(self, graph, vertices_names):
        return graph.add_vertices(vertices_names)
        
     
    def printGraphInfo(self, graph, function_name):
        if graph is None or graph.vcount() < 1:
            print("WARNING {}: graph is empty! Nothing to do".format(function_name), file = sys.stderr)
            return None
        else:
            print("{}: graph with: {} nodes and {} edges".format(function_name, graph.vcount(), graph.ecount()))


    def removeEdgesBetweenVerexTypes(self, graph, vtype):
        subg = graph.copy()

        for e in graph.es:
            sid = e.source
            tid = e.target
            stype = graph.vs[sid]['tag']
            ttype = graph.vs[tid]['tag']
            if stype in vtype and ttype in vtype:
                subg.delete_edges((sid, tid))

        return subg

    def removeVerticesByCentrality(self, graph, measure, vtype='persona', utype='organizzazione'):
        subgraph = graph.copy()

        if not bool(measure):
            print("removeVerticesByCentrality: using default measure wdegree")
            measure = 'wdegree'
        centrality = self.getCentralityVertices(graph, measure=measure)
        vert_to_keep = set()

        # Extract vertices of type vtype from centrality
        if bool(vtype):
            centrality = [x for x in centrality if graph.vs[x]['tag'] == vtype]

        for v in centrality:
            for u in graph.vs:
                if u['tag'] in utype:
                    for w in u.neighbors():
                        if w.index in vert_to_keep:
                            break 
                        elif v == w.index:
                            vert_to_keep.add(v)
                            subgraph.vs[v]['special2'] = vtype
                            break

        
        vertices_to_delete = [u.index for u in graph.vs if int(u.index) not in vert_to_keep and u['tag'] not in utype]
        subgraph.delete_vertices(vertices_to_delete)

        with open("persona_organizazione.txt", "w") as f:
            for v in vert_to_keep:
                print(80*"=", file = f)
                print("{}: ".format(graph.vs[v]['label']), end = " ", file = f)
                for u in graph.vs[v].neighbors():
                    print(u['label'], end = ", ", file = f)
                print("", file = f)

        print("removeVerticesByCentrality: found {} vertices to remove, keep {} vertices".format(len(vertices_to_delete), len(vert_to_keep)))
                        
        return subgraph

    def getGraphLayers(self, graph, selected_labels=None):
        """
        For each esisting label in graph.es['label'] return the subgraph containings
        only this label. 
        """
        graph_layers = {}
        newlabels = []
        labels = set(l for l in graph.es['label'])
        
        if bool(selected_labels):
            labels = [selected_labels]
            newlabels = labels

        if len(labels) > 1:
            for l in labels:
                for other in labels:
                    if l != other and l in other:
                        newlabels.append([l, other])
                        newlabels.remove([l])
                        newlabels.remove([other])

            for label in newlabels:
                l = '_'.join(label)
                graph_layers[l] = self.getSubgraphByEdgeTypes(graph, label)
        else:
            l = labels[0]
            graph_layers[l] = self.getSubgraphByEdgeTypes(graph, l)

        return graph_layers

    def getSubgraphByEdgeTypes(self, graph, edge_types, strict=True):

        if graph is None or graph.vcount() < 1:
            print("WARNING getSubgraphByEdgeTypes: graph is empty! Nothing to do", file = sys.stderr)
            return None
        else:
            print("getSubgraphByEdgeTypes: select a subgraph from a graph with: {} nodes and {} edges".format(graph.vcount(), graph.ecount()))

        subgraph = graph.copy()
        print("getSubgraphByEdgeTypes: select edges of type: {}".format(edge_types))
            
        if strict is True:
            edges_to_keep  = [e.index for e in graph.es if e['label'] in edge_types]
            subgraph = subgraph.subgraph_edges(edges_to_keep)
        else:
            vert_to_keep = set()
            for edge in edges_to_keep:
                sid  = graph.es[edge].source
                tid  = graph.es[edge].target
                vert_to_keep.add(sid) 
                vert_to_keep.add(tid)
                subgraph.vs[sid]['special'] = 'selected_edges'
                subgraph.vs[tid]['special'] = 'selected_edges'
                # Keep all neighbors
                nsid = set(graph.vs[sid].neighbors())
                ntid = set(graph.vs[tid].neighbors())
                vert_to_keep.update(nsid, ntid)
                vertices_to_delete = [v.index for v in graph.vs if int(v.index) not in vert_to_keep]
                print("getSubgraphByEdgeTypes: selected {} vertices to delete.".format(len(vertices_to_delete)))
                subgraph.delete_vertices(vertices_to_delete)

        print("getSubgraphByEdgeTypes: selected graph with {} vertices and {} edges".format(subgraph.vcount(), subgraph.ecount()))

        return subgraph


    def getSubgraphByVertices(self, graph, select='vertices', select_method='type',
                    vertex_types = [], vertex_attribute='', vertex_attribute_value='',
                    special_vertices_names= [], use_measure=False):
        """ Extract a subgraph of the input graph by selecting the vertex attribute (and value)
            or the edge_attribute (and value). If special_vertices is passed as a list
            of vertices names these vertices are also selected.
            select must be 'vertices' or 'edges' (default is 'vertices')
        """
        if graph is None or graph.vcount() < 1:
            print("WARNING getSubgraphByVertices: graph is empty! Nothing to do", file = sys.stderr)
            return None
        else:
            print("getSubgraphByVertices: select a subgraph from a graph with: {} nodes and {} edges".format(graph.vcount(), graph.ecount()))

        # Nodes can be selected by node type which is stored in vertex['tag']
        # or by passing an attribute with the correspending value        
        if select == 'vertices':

            if select_method == 'type':
                print("getSubgraphByVertices: select vertices of type: {}".format(vertex_types))
                subgraph = graph.copy()
                neighbors_set = set()
                vertices_to_keep = [v.index for v in graph.vs if v['tag'] in vertex_types or v['name'] in special_vertices_names]
                #for v in vertices_to_keep:
                #    for n in graph.vs[v].neighbors():
                #        neighbors_set.add(n.index)
                #        subgraph.vs[n.index]['special'] = 'label'

                vertices_to_keep += list(neighbors_set)
                vertices_to_delete = [v.index for v in graph.vs if (v.index not in vertices_to_keep)]
                subgraph.delete_vertices(vertices_to_delete)
                print("getSubgraphByVertices: selected graph with {} vertices and {}".format(subgraph.vcount(), subgraph.ecount()))

            elif select_method == 'attribute':
                subgraph = graph.copy()
                neighbors_set = set()
                print("getSubgraphByVertices: select vertices of type: {} {}".format(vertex_attribute, vertex_attribute_value))

                attribute_list = vertex_attribute_value.split()
                print("List of attributes: {}".format(attribute_list))

                vertices_to_keep = []

                for vl in attribute_list:
                    print("Search for nodes with {}".format(vl))
                    L = [v.index for v in graph.vs if (vl in v[vertex_attribute] or v['name'] in special_vertices_names)]
                    vertices_to_keep += L

                for v in vertices_to_keep:
                    if not bool(use_measure):
                        subgraph.vs[v]['special'] = 'label'
                    subgraph.vs[v]['special3'] = 'label'
                    for n in graph.vs[v].neighbors():
                        neighbors_set.add(n.index)


                vertices_to_keep += list(neighbors_set)
                vertices_to_delete = [v.index for v in graph.vs if (v.index not in vertices_to_keep)]
                subgraph.delete_vertices(vertices_to_delete)
                print("getSubgraphByVertices: selected graph with {} vertices and {}".format(subgraph.vcount(), subgraph.ecount()))

            else:
                print("WARNING: getSubgraphByVertices: select method not implemented: {}.".format(select_method), file = sys.stderr)

        return subgraph 

    
    def document_centrality(self, graph, label='docid', norm=True):
        c = [0] * graph.vcount()
        labelset = set()
        for v in graph.vs:
            labelset.add(v[label])
            v['labelset'] = [v[label]]
            for w in v.neighbors():
                if w[label] not in v['labelset']:
                    c[v.index] += 1
                    v['labelset'].append(w[label])
            # Remove this unused attribute
            v['labelset'] = ''

        if norm is True:
            norm = len(labelset)
            #print('NORMALIZATION DOCUMENT', norm)
            c = list(map(lambda x: x/norm, c))

        return c

    def getShortestPathsLenght(self, graph, source, target):
        if source != target:
            print("Not yet implemented")
            print("getShortestPathsLenght: source vertices must be equal to target vertices")
            sys.exit(111)
        paths = graph.shortest_paths_dijkstra(source=source, target=target, mode=ALL)
        vlist = [v['label'][:20] for v in graph.vs if v.index in source]
        siter = [val for val in zip(source, vlist, *(x for x in paths))]
        with open("paths.txt", "w") as f:
            print(tabulate(siter, headers=vlist, tablefmt="latex"), file=f)
        return siter


    @staticmethod
    def pairwise(iterable):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        a, b = tee(iterable)
        next(b, None)
        return zip(a, b)

    @staticmethod
    def graph_warning(graph, function_name):
        if graph is None or graph.vcount() < 1:
            print("WARNING {}: graph is empty! Nothing to do".format(function_name), file = sys.stderr)
            return None
        else:
            print("{}: graph with: {} nodes and {} edges".format(function_name, graph.vcount(), graph.ecount()))
        return

    @staticmethod
    def getVerticesFromLabels(graph, labels):
        if len(labels) < 2:
            print("WARNING getVerticesFromLabels: to compute a path you must specify at least two vertices,"
                  " only one was given. return None")
            return None
        
        vertices = [v.index for v in graph.vs for l in labels if l in v['label']]
        for v in vertices:
            graph.vs[v]['color'] = 'red'
        print(vertices)
        return vertices
    

    def getShortestPaths(self, graph, vertices):
        self.graph_warning(graph, "getShortestPaths")

        dpaths = {}
        edges  = {}
        for v in vertices:
            dpaths[v] = graph.get_shortest_paths(v, vertices)

        c = 0
        for u, paths in dpaths.items():
            for path in paths:
                for edge in self.pairwise(path):
                    eid = graph.get_eid(edge[0], edge[1])
                    edges.setdefault(u, [])
                    edges[u].append(eid)
                    c += self.setSpecialEdges(graph, edges[u])
            print("getShortestPaths: set {} edges".format(c))

        return (dpaths, edges) 

    def getCentralities(self, graph, normalized=True, store_table=True, num_elems=5, weight=True):

        if graph is None or graph.vcount() < 1:
            print("WARNING getCentralities: graph is empty! Nothing to do", file = sys.stderr)
            return None
        else:
            print("getCentralities: graph with: {} nodes and {} edges".format(graph.vcount(), graph.ecount()))

        
        
        N = graph.vcount()
        if normalized:
            normdeg  = N-1
            normbet  = ((N-1)*(N-2))/2.0
            normclo  = N-1
            normwdeg = N-1
            normdoc  = True
        else:
            normdeg  = 1
            normbet  = 1
            normclo  = 1
            normwdeg = 1
            normdoc  = False
        
        if weight is True and 'weights' in graph.es.attributes():
            w = 'weight'
        else:
            print("getCentralities: no weights")
            w = None
        
        if num_elems == 'all':
            num_elems = N

        use_document = False
        if 'docid' in graph.es.attributes():
            use_document = True

        # Must use an ordered dictionary to preserve key orders when sorting
        c = odict()
        c['degree']       = list(map(lambda x: x/normdeg, graph.degree()))
        c['wdegree']      = list(map(lambda x: x/normwdeg, self.weigthed_degree(graph)))
        c['betweenness']  = list(map(lambda x: x/normbet, graph.betweenness(weights=w)))
        c['closeness']    = list(map(lambda x: x/normclo, graph.closeness(weights=w)))
        if use_document:
            c['document']     = list(map(lambda x: x, self.document_centrality(graph, norm=normdoc)))

        if 'label' in graph.vs.attributes():
            for v in graph.vs:
                vlabel = [x[:20] for x in graph.vs['label']]

        if store_table:
            idx   = [i for i in range(graph.vcount())]
            siter = [vals for vals in zip(idx, vlabel, *(c[k] for k in c))]
            lenc  = len(c)
           
            b = {} 
            head = [k for k in c]
            headers = ['id', 'name'] + head
            for i in range(2, len(headers)):
                s = sorted(siter, key = lambda x: x[i], reverse=True)[:num_elems]
                # Set best nodes
                for k in s:
                    b.setdefault(headers[i], [])
                    b[headers[i]].append(list(k)[0])
                # Dump a table
                ch = list(headers)
                ch[i] = ch[i].upper()
                print(tabulate(s, ch, tablefmt="latex"))
                print()

        b['union'] = list(set.union(*[set(x) for x in b.values()]))
        b['intersection'] = list(set.intersection(*[set(x) for x in b.values()]))

        return b
        

    def getCentralityVertices(self, graph, measure=None, normalized=True, color=None, docid=None, 
                              return_value=False, return_pair=False, return_names=False, return_num='all',
                              weight=True):
        """ Return a list of vertices indices sorted by decreasing centrality value, 
            centrality is given by measure [degree, wdegree, betweenness, closeness, document]
            If return_value is True return the centrality value for each v in graph
            If return_pair is True return a list of pairs: (vertex_is, centrality_value)
            if return_names is True return names (g.vs[vertex_id]['name']) instead of indices
            if return_num is given as N (integer value) return only the first N elements 
            If docid is given, only vertices with that docid are returned.
            If color is given vertices color attribute will be set to that color
        """
        N = graph.vcount()
        if normalized:
            normdeg  = N-1
            normbet  = ((N-1)*(N-2))/2.0
            normclo  = N-1
            normwdeg = N-1
            normdoc  = True
        else:
            normdeg  = 1
            normbet  = 1
            normclo  = 1
            normwdeg = 1
            normdoc  = False
        
        if weight is True:
            w = 'weight'
        else:
            w = None

        if measure == 'degree' or measure is None:
            centrality = graph.degree()
            normcent   = normdeg
        elif measure == 'wdegree' or measure is None:
            centrality = self.weigthed_degree(graph)
            normcent   = normwdeg
        elif measure == 'betweenness':
            centrality = graph.betweenness(weights=w)
            normcent   = normbet
        elif measure == 'closeness':
            centrality = graph.closeness(weights=w)
            normcent   = normclo
        elif measure == 'document':
            centrality = self.document_centrality(graph, norm=normdoc)

        else:
            print("WARNING getCentralityVertices: {} not implemented, return None".format(measure), file = sys.stderr)
            return None

        if normalized:
            centrality = list(map(lambda x: x/normcent, centrality))


        ids    = [x for x in range(graph.vcount())]
        siter  = zip(ids, centrality)

        print("getCentralityVertices: use centrality: {}, num vertices: {}, num edges {}".format(measure, graph.vcount(), graph.ecount()))

        # Just return the choseen centrality
        if return_value is True:
            print("getCentralityVertices: return a list with the centrality value for each vertex id")
            vertices = centrality
        # Return vertex, value pair ordered by decreasing centrality values
        elif return_pair is True:
            print("getCentralityVertices: return a list of tuples vertex_id, centrality_value, ordered by decreasing centrality values")
            vertices = [(x, y) for x,y in sorted(siter, key = lambda x: x[1], reverse=True)]
        # Return vertices ordered by decreasing centrality values
        elif return_names:
            print("getCentralityVertices: return a list of vertices names ordered by decreasing centrality values")
            vertices = [graph.vs[x]['name'] for x, y in sorted(siter, key = lambda x: x[1], reverse=True)]
        else:
            print("getCentralityVertices: return a list of vertices id ordered by decreasing centrality values")
            vertices = [x for x, y in sorted(siter, key = lambda x: x[1], reverse=True)]

        if docid is not None:
            vertices = [v for v in vertices if v['docid'] == docid]

        print("getCentralityVertices: centrality computed for {} elements".format(len(vertices)))

        if return_num != 'all':
            return vertices[0:int(return_num)]

        return vertices


    @staticmethod
    def shapeVertex(graph, v_idx, shape):

        graph.vs[v_idx]['size']       = shape['size']
        graph.vs[v_idx]['label_size'] = shape['label_size']

        if graph.vs[v_idx]['tag'] in ORGANIZZAZIONI:
            graph.vs[v_idx]['shape'] = 'square' 
        else:
            graph.vs[v_idx]['shape'] = shape['shape'] 

        return graph.vs[v_idx]

    @staticmethod
    def shapeEdge(graph, edge_idx, color, width):

        graph.es[edge_idx]['color'] = color 
        graph.es[edge_idx]['width'] = width
        
        return graph.es[edge_idx]
        

    def shapeGraph(self, graph, edge_colors = None, edge_widths = None, centrality = None, 
                   shape=None, no_shape_edge=False, no_shape_vertex=False):
        """
            If edge_colors is given as a list of colors this will be used as a palette for edges
            If edge_widths is given as a list of thickness this will be used to set edge thickness
            If centrality is given vertices are shaped by the values in centrality
        """

        use_colormap   = (bool(edge_colors))
        use_widthmap   = (bool(edge_widths))
        use_centrality = (bool(centrality))


        print("shapeGraph: num vertices: {}, num edges: {}, " \
               "color map for edges: {}, width map for edges: {}, centrality vertices: {}"\
              .format(graph.vcount(), graph.ecount(), use_colormap, use_widthmap, use_centrality))

        # SHAPE EDGES
        counte = 0
        if use_widthmap or use_colormap:
            # Manage a graph with 0 edges
            if bool(graph.es['weight']):
                max_weight_value = max(graph.es['weight'])
            else:    
                max_weight_value = 1

            for edge in graph.es:
                if 'special' in graph.es.attributes() and bool(graph.es[edge.index]['special']):
                    # special edges are set in setSpecialEdges, nothing to do here
                    pass
                else:
                    myweight = edge['weight']/max_weight_value
                    if use_colormap: color = edge_colors[(math.ceil(len(edge_colors)*myweight) - 1)]
                    if use_widthmap: width = edge_widths[(math.ceil(len(edge_widths)*myweight) - 1)]
                    self.shapeEdge(graph, edge.index, color, width)
                    counte += 1
                    
        # Use defaults
        elif no_shape_edge is True:
            pass
        else:
            for edge in graph.es:
                if 'special' in graph.es.attributes() and bool(graph.es[edge.index]['special']):
                    # special edges are set in setSpecialEdges, nothing to do here
                    pass
                else:
                    self.shapeEdge(graph, edge.index, 'darkgray', '0.8')
                    counte += 1

        ### SHAPE VERTICES
        if shape is None:
            if graph.vcount() < 100:
                shape = large_shape
            else:
                shape = default_shape
        
        count = 0
        count_special = 0
        if use_centrality:
            maxcen = max(centrality)
            avgcen = mean(centrality)

            print("shapeGraph: shape vertices by centrality measure, max {}, mean {}".format(maxcen, avgcen))
            for v in graph.vs:
                count += 1
                if 'special' in graph.vs.attributes() and bool(v['special']):
                    count_special +=1
                    self.shapeVertex(graph, v.index, shape['special'])
                elif 'special2' in graph.vs.attributes() and bool(v['special2']):
                    count_special +=1
                    self.shapeVertex(graph, v.index, shape['special2'])
                elif 0.5*maxcen < centrality[v.index] <= maxcen:
                    self.shapeVertex(graph, v.index, shape['above'])
                elif 0.25*maxcen < centrality[v.index] <= 0.5*maxcen:
                    self.shapeVertex(graph, v.index, shape['mean'])
                elif 0.05*maxcen < centrality[v.index] <= 0.25*maxcen:
                    self.shapeVertex(graph, v.index, shape['below'])
                elif 0 <= centrality[v.index] <= 0.05*maxcen:
                    self.shapeVertex(graph, v.index, shape['min'])
                else:
                    print("WARNING shapeGraph: error shaping the graph, added SQUARE NODES.", file=sys.stderr)
                    self.shapeVertex(graph, v.index, shape['zero'])
        elif no_shape_vertex is True:
            pass
        else:
            print("shapeGraph: shape vertices by defalut")
            for v in graph.vs:
                if 'special' in graph.vs.attributes() and bool(v['special']):
                        count_special +=1
                        self.shapeVertex(graph, v.index, shape['special'])
                elif 'special2' in graph.vs.attributes() and bool(v['special2']):
                    count_special +=1
                    self.shapeVertex(graph, v.index, shape['special2'])
                else:
                    count += 1
                    self.shapeVertex(graph, v.index, shape['mean'])

        print("shapeGraph: shaped {} vertices and {} edges ({} special).".format(count, counte, count_special))
                
        return True

    def setSpecialEdges(self, graph, edges, name='special', color=special_color, width='5'):
        # SHAPE EDGES
        # If edge labels are give only these are colored
        count = 0
        for edge in graph.es:
            if edge.index in edges:
                graph.es[edge.index]['color']   = color 
                graph.es[edge.index]['width']   = width
                graph.es[edge.index]['special'] = name
                count += 1
        
        return count


    # The input list of special nodes must be a list of index in the original data struct
    # because the id of igraph vertices changes on each subgraph selected.
    def setSpecialVertices(self, graph, vertices, name, set_by_name = False, set_by_index = False):

        if len(vertices) > 0:

            if set_by_name:
                set_by_index = False
                print("setSpecialVertices: using vertices names to set special vertices")
            elif set_by_index:
                set_by_name = False
                print("setSpecialVertices: using vertices indices to set special vertices")
            else:
                print("WARNING setSpecialVertices: either set_by_name or set_by_index must be True. Nothing will be done")
                return False

            count = 0
            for v in graph.vs:
                vname = v['name']
                vidx  = v.index

                if set_by_index is True:  vtag = vidx
                elif set_by_name is True: vtag = vname

                if vtag in vertices:
                    graph.vs[vidx]['special'] = name
                    graph.vs[vidx]['label_color'] = special_color
                    print("setSpecialVertices: set vertex: {}".format(graph.vs[vidx]['label']))
                    count += 1

            print("setSpecialVertices: set {} special vertices, tot vertices in g: {}, percentage: {}"
                   .format(count, graph.vcount(), 100*float(count)/float(graph.vcount())))
            return True
                        
        else:
            return False
                
    def getGraphKcore(self, graph, K):
        return graph.k_core(K)

  
    def getClusters(self, g, attribute, cmeasure):
        """
            Just a wrapper for igraph Clustering, return a list of lists, 
            each list containing the vertices id of one cluster
        """
        self.graph_warning(g, "getClusters")
        print("Get cluster: clustering with {}".format(attribute))
        g.simplify(combine_edges=dict(weight=sum))

        # Reset colors, colors are given by the clustering algorithms
        g.vs['color'] = 0
        g.es['width'] = 0
        if bool(attribute):
            cl = VertexClustering.FromAttribute(g, attribute)
        else:
            cl = g.community_multilevel()

        # From Gabor Csardi
        # To have a nice plot: Remove the edges across multiple communities, 
        # calculate the layout without these edges, and then use it for the original graph.
        crossing = cl.crossing()
        gcopy = g.copy()
        edges_to_delete = []
        for e in g.es:
            if crossing[e.index] is True:
                edges_to_delete.append(e.index)

        gcopy.delete_edges(edges_to_delete)
        nocross_layout = gcopy.layout()

        print("getClusters: number of crossing edges: {}".format(len(edges_to_delete)))

        # Shape the graph
        self.setGraphOptions(g, centrality_measure=cmeasure)
        subg = cl.subgraphs()
        count = 0
        for s in subg:
            print("============================================================")
            print("Setting cluster number: {}, color: {}".format(count, count))
            # Skip centralization for small graphs, centralizations measures requires at least 3 vertices
            if s.vcount() < 3:
                self.setGraphOptions(s, centralization=False)
            else:
                self.setGraphOptions(s, centrality_measure=cmeasure)
            print("============================================================")
            count += 1

        count = 0
        for e in g.es:
            if crossing[e.index] is True:
                g.es[e.index]['width'] = 1.0
                g.es[e.index]['color'] = 'black'
                count += 1
        print("getClusters: colored {} edges".format(count))

        plot(cl, 'clusters.pdf', bbox = (4000, 4000), margin = 300, layout = nocross_layout)
        print("getClusters: graph plotted to file clusters.pdf")


        #group_markers = [(clique, "gray") for clique in mcliques]
        #plot(g2, mark_groups=group_markers)
    

       # for c in cl:
       #     if len(c) < 20:
       #         for v in c:
       #             c.remove(v)
       # g.vs['size'] = 5
       # g.vs['label_size'] = 2
       # for edge in g.es:
       #     if crossing[edge.index] is True:
       #         g.es[edge.index]['color']   = "red"
       #         g.es[edge.index]['width']   = 1
       #         g.es[edge.index]['label']   = "cross"
       #     else:
       #         pass

                    
        #g.es['width'] = [5*int(x == 'true') for x in crossing]
        #plot(cl, "PIPPO.pdf")
        sys.exit()
        return



 

    def getLargestComponent(self, graph):
        print("getLargestComponent: graph with num vertices: {}, num edges {}".format(graph.vcount(), graph.ecount()))
        # Find connected components
        cl = graph.clusters()
        lcc = cl.giant()
        print("getLargestComponent: giant componenet with num vertices: {}, num edges {}".format(lcc.vcount(), lcc.ecount()))
        return lcc


    def simplePlot(self, graph, glayout=None, glayout_weight=True, outfile="simple_plot.pdf", set_cluster_colors=True, colors = []):
        if graph is None or graph.vcount() < 1:
            print("WARNING simple_plot: graph is empty! Nothing to do", file = sys.stderr)
            return None

        if glayout is None:
            glayout='auto'
        elif glayout == 'bipartite':
            print("simplePlot: bipartite layout")
            if 'tag' in graph.vs.attributes():
                for v in range(graph.vcount()):
                    graph.vs[v]['type'] = ('persona' in graph.vs[v]['tag'])
                    graph.vs[v]['label_angle'] = -math.pi
                    graph.vs[v]['label_dist'] = 10
                    graph.vs[v]['size'] = 50
            else:
                graph.vs['type'] = None
                print("simplePlot: first vertex of each tuple is of type 1 second vertex of type 2")
                for e in graph.es:
                    sid = e.source
                    tid = e.target
                    if graph.vs[sid]['type'] is None:
                        graph.vs[sid]['type'] = 0
                        graph.vs[sid]['shape'] = 'circle'
                    if graph.vs[tid]['type'] is None:
                        graph.vs[tid]['type'] = 1
                        graph.vs[tid]['shape'] = 'square'
                    #print(graph.vs[sid]['type'], graph.vs[tid]['type'])

                graph.vs['size'] = 50

            # This bipartite layout is not configurable
            #glayout = graph.layout_bipartite(types='type', vgap=0.1, hgap=100)
        
        if bool(glayout_weight):
            print("simplePlot: weigths are set so the layout will be fr")
            glayout = graph.layout('fr', weights='weight')


        # Make a graph clustering for coloring
        cl = None
        set_vertex_order = False
        vorder = None

        if 'docid' in graph.vs.attributes():
            cl = Clustering(graph.vs['docid'])
            membership = cl.membership
            graph.vs['color'] = [color_map1[i] for i in membership]
            set_vertex_order = True
            cl_document = VertexClustering(graph, membership)
            # Print number of nodes in each cluster
            if bool(cl):
                clen=len(cl)
                table = []
                for i in range(clen):
                    if not bool(cl[i]): continue
                    v0 = cl[i][0]
                    v0_dre = graph.vs['docid'][v0]
                    print(v0_dre, datamanager.document[v0_dre]['dre'], cl.size(i))
                    table.append((datamanager.document[v0_dre]['dre'], v0_dre, cl.size(i), color_map1[i]))
                print(tabulate(table, numalign="right", floatfmt=".2f", tablefmt="latex"), end = '\n\n')
        elif 'tag' in graph.vs.attributes():
            cl = Clustering(graph.vs['tag'])
            membership = cl.membership
            graph.vs['color'] = [color_map1[i] for i in membership]
            cl_document = VertexClustering(graph, membership)
        elif 'type' in graph.vs.attributes():
            cl = Clustering(graph.vs['type'])
            membership = cl.membership
            graph.vs['color'] = [color_map1[i] for i in membership]
            cl_document = VertexClustering(graph, membership)
            glayout = 'auto'
        else:
            print("WARNING simplePlot: no vertex attribute to set colors.", file = sys.stderr)
            set_cluster_colors = False
            cl_document = graph



        if set_cluster_colors:
            vcolor = graph.vs['color']
            if bool(colors):
                vcolor = colors
        else:
            vcolor = 'darkolivegreen3'

        if set_vertex_order:
            vorder = [x[0] for x in sorted(enumerate(graph.vs['docid']), key = lambda x: x[1])]

        plot(cl_document, outfile, bbox = (4000, 4000), margin = 300, layout = glayout, vertex_order = vorder, vertex_color = vcolor)
        print("simplePlot: plotted a graph with {} nodes and {} edges to file {}".format(graph.vcount(), graph.ecount(), outfile))

        return None

    # Just a wrapper
    def writeGraphML(self, graph, fout="output_graph.graphml.gz", zip_comp_lvl=1):
        self.printGraphInfo(graph, "writeGraphML")
        print("writing to file: {}".format(fout))
        return graph.write_graphmlz(fout, compresslevel=zip_comp_lvl)

    
    def setGraphOptions(self, graph, centralization=True, sum_weights=False, centrality_measure=None, weight_centrality=True,
                        paths=None, edge_colors = [], edge_widths = []):
        """
        """

        if graph is None or graph.vcount() < 1:
            print("WARNING setGraphOptions: graph is empty! Nothing to do", file = sys.stderr)
            return None

        if centralization is True:
            centralization = self.getGraphCentralization(graph, get_all=True, get_avg_paths=True)

        # get centrality values
        if centrality_measure is None:
            centrality = []
        else:
            centrality = self.getCentralityVertices(graph, measure=centrality_measure, return_value=True, weight=weight_centrality)
            print("setGraphOptions: use centrality measure: {}, num vertices: {}".format(centrality_measure, len(centrality)))

        #maxcen = max(centrality)
        #graph.vs['centrality'] = list(map(lambda x: int(1000*(maxcen - x)), centrality))
        #print(graph.vs['centrality'])
        
        
        if sum_weights is True:
            graph.simplify(combine_edges=dict(weight=sum))
            graph.es['curved'] = False
            graph.es['color']  = 'darkgrey2'
            graph.es['width']  = '0.8'
            c = 0
            if paths:
                graph.es['width']  = '0.5'
                edges = {}
                for u, mypaths in paths.items():
                    for path in mypaths:
                        for edge in self.pairwise(path):
                            eid = graph.get_eid(edge[0], edge[1])
                            edges.setdefault(u, [])
                            edges[u].append(eid)
                            c += self.setSpecialEdges(graph, edges[u])
                print("setSpecialEdges: set {} special edges".format(c))

                
        if edge_colors == 'use_default': edge_colors = colormap_grey
        if edge_widths == 'use_default': edge_widths = default_widthmap

        # Set shape
        shape=None
        if centrality_measure == 'document': 
            shape = shape_document 
            shape = paths_shape

        self.shapeGraph(graph, edge_colors=edge_colors, edge_widths=edge_widths, centrality=centrality, shape=shape) 

        return graph


    def weigthed_degree(self, graph):

        degree = [0]*graph.vcount()
        for e in graph.es:
            sid  = graph.es[e.index].source
            tid  = graph.es[e.index].target
            degree[sid] += float(e['weight'])
            degree[tid] += float(e['weight'])

        return degree


    def getGraphCentralization(self, graph, get_all=False, get_avg_paths=False, fout=None):

        print("getGraphCentralization: graph with {} nodes and {} edges".format(graph.vcount(), graph.ecount()))

        gc = {}

        gc['diameter']  = graph.diameter()
        gc['density']   = graph.density()
        gc['mindeg']    = min(graph.degree())
        gc['maxdeg']    = max(graph.degree())
        gc['avgdeg']    = mean(graph.degree())

        if get_avg_paths is True:
            paths = graph.shortest_paths()
            paths_len = sum([len(p) for p in paths])
            fpaths = [p for plist in paths for p in plist if p != float('Inf')]
            avg_path_len = float(sum(fpaths))/float(len(fpaths))
            print("getGraphCentralization: total number of paths: {}".format(paths_len))
            print("getGraphCentralization: total number of finite paths: {}".format(len(fpaths)))
            print("getGraphCentralization: average path length: {}".format(avg_path_len))
            gc['avg_paths'] = avg_path_len

        if get_all is True:
            gc['d_centralization'] = self.degree_centralization(graph)
            gc['b_centralization'] = self.betweenness_centralization(graph)
            gc['c_centralization'] = self.closeness_centralization(graph)
            gc['w_centralization'] = self.wdegree_centralization(graph)

        stdout_orig = sys.stdout
        if fout is not None:
            sys.stdout  = open(fout, 'w')
        else:
            fout = sys.stdout
            
        print("graph diameter: {}".format(gc['diameter']), file = fout)
        print("graph density:  {}".format(gc['density']), file = fout)
        print("graph avgdeg: {}".format(gc['avgdeg']), file = fout)
        print("graph mindeg: {}".format(gc['mindeg']), file = fout)
        print("graph maxdeg: {}".format(gc['maxdeg']), file = fout)
        if get_all is True:
            print("graph d_centralization: {}".format(gc['d_centralization']), file = fout)
            print("graph b_centralization: {}".format(gc['b_centralization']), file = fout)
            print("graph c_centralization: {}".format(gc['c_centralization']), file = fout)
            print("graph w_centralization: {}".format(gc['w_centralization']), file = fout)

        sys.stdout = stdout_orig 

        return gc




    def betweenness_centralization(self, graph, btw=None):
        vnum = graph.vcount()
        if vnum < 3:
            raise ValueError("betweenness_centralization: graph must have at least three vertices")
        denom = (vnum-1)*(vnum-2)
        
        if btw is not None: 
            temparr = [2*i/denom for i in btw]
        else:
            temparr = [2*i/denom for i in graph.betweenness()]

        max_temparr = max(temparr)
        return sum(max_temparr-i for i in temparr)/(vnum-1)
    
    def wdegree_centralization(self, graph, deg=None):
        vnum = graph.vcount()
        if vnum < 3:
            raise ValueError("degree_centralization: graph must have at least three vertices")
        denom = (vnum-1)*(vnum-2)
        
        if deg is not None: 
            temparr = deg
        else:
            temparr = [i for i in self.weigthed_degree(graph)]

        max_temparr = max(temparr)
        return sum(max_temparr-i for i in temparr)/denom

    def degree_centralization(self, graph, deg=None):
        vnum = graph.vcount()
        if vnum < 3:
            raise ValueError("degree_centralization: graph must have at least three vertices")
        denom = (vnum-1)*(vnum-2)
        
        if deg is not None: 
            temparr = deg
        else:
            temparr = [i for i in graph.degree()]

        max_temparr = max(temparr)
        return sum(max_temparr-i for i in temparr)/denom

    def closeness_centralization(self, graph, clo=None):
        vnum = graph.vcount()
        if vnum < 3:
            raise ValueError("closeness_centralization: graph must have at least three vertices")
        denom = (vnum-1)*(vnum-2)/(2*vnum - 3)
        
        if clo is not None: 
            temparr = clo
        else:
            temparr = [i for i in graph.closeness()]

        max_temparr = max(temparr)
        return sum(max_temparr-i for i in temparr)/denom




   # def getClusters(self, g):
   #     # CLUSTERING WITH LABEL PROPAGATION
   #     vorder = [x[0] for x in sorted(enumerate(g.degree()), key = lambda x: x[1])]
   #     clusters = g.community_label_propagation(weights='weight')
   #     print(clusters)
   #     layout = g.layout("fr", weights = 'weight')
   #     g.es['curved'] = False
   #     g.es['color']  = 'darkgrey2'
   #     g.es['width']  = '1'
   #     g.simplify(combine_edges=dict(weight=sum))
   #     #mgroups = [(list(clusters[0]), 'red'), (list(clusters[1]), 'blue'), (list(clusters[2]), 'orange'), (list(clusters[3]), 'green'), (list(clusters[4]), 'yellow'), (list(clusters[5]), 'pink')]
   #     plot(clusters, "clusters.pdf", bbox = (4000, 4000), margin = 100, layout = layout, marks_group = True, vertex_order = vorder)
   #     return clusters



    #def do_plot_new(self, g, document):
    #    
    #    if g is None or g.vcount() < 1:
    #        print("WARNING do_plot_new: graph is empty! Nothing to do", file = sys.stderr)
    #        return None
    #    
    #    maxdeg = max(g.degree())
    #    mindeg = min(g.degree())
    #    avgdeg = mean(g.degree())

    #    print("Generating attributes for plotting")
    #    #print("max degree {0}, min degree {1}, average degree {2}".format(maxdeg, mindeg, avgdeg))


    #    for i in range(0, len(g.vs)):
    #        v   = g.vs[i].index
    #        deg = g.degree()[i]


    #        if (g.vs[i]['special']):
    #            #print("SPECIAL NODE:", g.vs[i]['label'])
    #            g.vs[i]['size']  = 80
    #            #g.vs[i]['label'] = v
    #            g.vs[i]['shape'] = 'circle'
    #            g.vs[i]['label_size'] = 30
    #            
    #        elif (deg >= 0.75*maxdeg):
    #            g.vs[i]['size']  = 50
    #            #g.vs[i]['label'] = v
    #            g.vs[i]['shape'] = 'circle'
    #            g.vs[i]['label_size'] = 20

    #        elif (deg >= 0.5*avgdeg):
    #            g.vs[i]['size']  = 30
    #            #g.vs[i]['label'] = v
    #            g.vs[i]['shape'] = 'circle'
    #            g.vs[i]['label_size'] = 10

    #        elif (deg >= 0.25*avgdeg):
    #            g.vs[i]['size'] = 20
    #            #g.vs[i]['label'] = i
    #            g.vs[i]['label_size'] = 10
    #            g.vs[i]['shape'] = 'circle'
    #        
    #        else:
    #            g.vs[i]['size'] = 10
    #            #g.vs[i]['label'] = None
    #            g.vs[i]['shape'] = 'circle'
    #            g.vs[i]['label_size'] = 10


    #    # ONE SPCIFIC NODE
    #    gone = g.copy()
    #    edges_to_delete = []
    #    vertices_to_delete = []
    #    vertices_to_add = []
    #    for i in range(len(g.es)):
    #        sid = gone.es[i].source
    #        tid = gone.es[i].target
    #        if ('Schiavone' in g.vs[sid]['label']) or ('Schiavone' in g.vs[tid]['label']):
    #            if (g.vs[sid]['special']):
    #                gone.vs[sid]['size']  = 120
    #                gone.vs[sid]['shape'] = 'circle'
    #                gone.vs[sid]['label_size'] = 50
    #            if (g.vs[tid]['special']):
    #                gone.vs[tid]['size']  = 120
    #                gone.vs[tid]['shape'] = 'circle'
    #                gone.vs[tid]['label_size'] = 50
    #            else:
    #                gone.vs[sid]['size']  = 80
    #                gone.vs[sid]['shape'] = 'circle'
    #                gone.vs[sid]['label_size'] = 50
    #                gone.vs[tid]['size']  = 80
    #                gone.vs[tid]['shape'] = 'circle'
    #                gone.vs[tid]['label_size'] = 50
    #            vertices_to_add.append(gone.vs[sid])
    #            vertices_to_add.append(gone.vs[tid])
    #        else:
    #            edges_to_delete.append(gone.es[i])
    #            vertices_to_delete.append(gone.vs[sid])
    #            vertices_to_delete.append(gone.vs[tid])

    #    #gone.delete_edges(edges_to_delete)
    #    #gone.delete_vertices(vertices_to_delete)
    #    gone1 = gone.subgraph(vertices_to_add)
    #    gone1.es['color'] = 'black'
    #    gonelayout = gone1.layout("fr", weights = 'weight')
    #    print("Creating file: campania_one_node.pdf")
    #    plot(gone1, "campania_one_node.pdf", bbox = (4000, 3000), margin = 300, layout = gonelayout)

    #    layout = g.layout("fr", weights="weight")

    #    # CLUSTER SENTENZE
    #    cl = Clustering(g.vs['docid'])
    #    membership = cl.membership
    #    cl_document = VertexClustering(g, membership)
    #    # Questo viene sbagliato! Come mai??
    #    #cl_document = VertexClustering(g).FromAttribute(g, 'docid')
    #    vorder = [x[0] for x in sorted(enumerate(g.vs['docid']), key = lambda x: x[1])]
    #    print("Creating file: campania_document.pdf")
    #    plot(cl_document, "campania_document.pdf", bbox = (4000, 4000), margin = 100, \
    #         layout = layout, vertex_order = vorder, vertex_color = g.vs['color'])

    #    for i in range(len(cl_document)):
    #        for dre, val in document.items():
    #            if (i == val[0]):
    #                thissent = dre
    #                break
    #        print("Number of nodes in sentence {0}: {1},\tcolor = {2}, name = {3}"\
    #              .format(i, len(cl_document[i]), color_map[i], thissent))

    #    # LINKS COLLEGATO
    #    gcoll = g.copy()
    #    for i in range(len(gcoll.es)):
    #        if (gcoll.es[i]['label'] == 'collegato'):
    #            gcoll.es[i]['color'] = 'black'
    #            gcoll.es[i]['width'] = 3
    #        else:
    #            gcoll.es[i]['color'] = 'grey'
    #            gcoll.es[i]['width']  = 0.5
    #            gcoll.es[i]['label'] = ''
    #    vorder = [x[0] for x in sorted(enumerate(gcoll.vs['docid']), key = lambda x: x[1])]
    #    print("Creating file: campania_collegato.pdf")
    #    plot(gcoll, "campania_collegato.pdf", bbox = (4000, 4000), margin = 100, layout = layout, vertex_order = vorder)


    #    # LINKS PARENTS
    #    gparent = g.copy()
    #    for i in range(len(gparent.es)):
    #        if (gparent.es[i]['label'] == 'parente'):
    #            gparent.es[i]['color'] = 'black'
    #            gparent.es[i]['width'] = 3
    #        else:
    #            gparent.es[i]['color'] = 'grey'
    #            gparent.es[i]['width']  = 0.5
    #            gparent.es[i]['label'] = ''
    #    vorder = [x[0] for x in sorted(enumerate(gparent.vs['docid']), key = lambda x: x[1])]
    #    print("Creating file: campania_parente.pdf")
    #    plot(gparent, "campania_parente.pdf", bbox = (4000, 4000), margin = 100, layout = layout, vertex_order = vorder)



    #    # SUM WEIGHTS
    #    g.simplify(combine_edges=dict(weight=sum))
    #    #g.to_undirected()

    #    # CLUSTERING WITH LABEL PROPAGATION
    #    g2_vs = g.vs.select([v for v, b in enumerate(g.degree()) if b > 0])
    #    g2 = g.subgraph(g2_vs)
    #    vorder = [x[0] for x in sorted(enumerate(g.degree()), key = lambda x: x[1])]
    #    clusters = g2.community_label_propagation(weights='weight')
    #    layout = g2.layout("fr", weights = 'weight')
    #    print("Creating file: campania_clusters.pdf")
    #    mgroups = [(list(clusters[0]), 'red'), (list(clusters[1]), 'blue'), (list(clusters[2]), 'orange'), (list(clusters[3]), 'green'), (list(clusters[4]), 'yellow'), (list(clusters[5]), 'pink')]
    #    plot(clusters, "campania_clusters.pdf", bbox = (4000, 4000), margin = 100, layout = layout, marks_group = True, vertex_order = vorder)

    #def getGraphStats(self, g, gname, level='basic', length=5):    
    #    """ 
    #    """
    #    if g is None or g.vcount() < 1:
    #        print("WARNING: getGraphStats: ds['nodes'] is empty cannot create the graph!")
    #        return None
    #    
    #    best   = {'deg': set(), 'bet': set(), 'clo': set(), 'eig': set(), 'unodes': set(), 'inodes': set()}
    #    stats  = {'count': [], 'deg': [], 'bet': [], 'clo': [], 'eig': []}
    #    bstats = {'diameter': 0, 'density': 0, 'maxdeg': 0, 'mindeg': 0, 'avgdeg': 0}
    #    slists  = []
    #    table  = []

    #    # append grah name to the output file
    #    outfile = self.statfile + '_' + gname

    #    bstats['diameter'] = g.diameter()
    #    bstats['density']  = g.density()
    #    bstats['maxdeg']   = max(g.degree())
    #    bstats['mindeg']   = min(g.degree())
    #    bstats['avgdeg']   = mean(g.degree())

    #    cstats = {'bstats': bstats, 'stats': stats, 'best': best}

    #    # Basic level stops here
    #    if level == 'basic':
    #        return bstats
    #    elif level == 'complete':
    #        pass
    #    else:
    #        print("WARNING getGraphStats: level not defined, defaults to basic", file = sys.stderr)
    #        return bstats

    #    with open(outfile, 'w') as f:
    #        print("WHOLE NETWORK METRICS", file = f) 
    #        for k, v in bstats.items():
    #            print("Graph {}:\t{:10.4f}".format(k, v), file = f)
    #        print("\n\n", file = f)

    #    slists.append(range(g.vcount()))
    #    slists.append(g.degree())
    #    slists.append(g.betweenness(weights='weight'))
    #    slists.append(g.closeness(weights='weight'))
    #    slists.append(g.eigenvector_centrality(weights='weight'))

    #    statnames = {1:'DEGREE CENTRALITY', 2:'BETWEENNESS CENTRALITY', \
    #                 3:'CLOSENESS CENTRALITY', 4:'EIGENVECTOR CENTRALTY'} 
    #    

    #    # Store most important nodes in sets

    #    headers = ["node", "deg", "bet", "clo", "eig"]
    #    siter = zip(slists[0], slists[1], slists[2], slists[3], slists[4])


    #    print("Doing some centrality measure computation, creating file: ", outfile)
    #    with open(outfile, 'a') as f:
    #        for sk in statnames.keys():
    #            table = []
    #            title = "NODE SCORE SORTED BY {0}"
    #            print(80*'=', file=f)
    #            print(title.format(statnames[sk]), file = f)
    #            siter = zip(slists[0], slists[1], slists[2], slists[3], slists[4])
    #            # INCLUDE ONLY THE BEST 5 NODES  --> [:5]
    #            for idx, dval, bval, cval, egval in sorted(siter, key = lambda x: x[sk], reverse=True)[:length]:
    #                table.append([g.vs[idx]['label'], dval, bval, cval, egval])
    #                best[headers[sk]].add(idx)        

    #            print(tabulate(table, headers = headers, numalign="right", floatfmt=".5f"), end = '\n\n', file=f)

    #    sets   = [best['deg'], best['bet'], best['clo'], best['eig']]
    #    unodes = set.union(*sets)
    #    
    #    inodes = set.intersection(*sets)
    #    i = len(sets)-1
    #    stop = -1
    #    while((i > 0) and (stop < 0)):
    #        combs = comb(sets, i)
    #        i = i - 1
    #        for tuples in combs:
    #            inodes = set.intersection(*tuples)
    #            if inodes:
    #                #print(inodes)
    #                stop = 1
    #                break
    #            else:
    #                pass 
    #    
    #    with open(outfile, 'a') as f:
    #        print("Union nodes: ", file = f)
    #        for node in unodes:
    #            print("{0}".format(g.vs[node]['label']), end =", ", file = f)
    #        print(file = f)

    #        print("Intersection nodes: ", file = f)
    #        for node in inodes:
    #            print("{0}".format(g.vs[node]['label']), end = ", ", file = f)
    #        print(file = f)

    #    best['deg']    = list(best['deg'])
    #    best['bet']    = list(best['bet'])
    #    best['clo']    = list(best['clo'])
    #    best['eig']    = list(best['eig'])
    #    best['unodes'] = list(unodes)
    #    best['inodes'] = list(inodes)
    #    return cstats
