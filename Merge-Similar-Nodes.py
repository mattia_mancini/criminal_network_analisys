from lib import mergeUtils, logger, datamanager, datamanager_legacy, cacheManager
import sys
import time
import json

log = logger.getLogger(__file__)

# def printstats(outfile):
#     Q_OLD_CAMPANIA = 'MATCH (n)-[a]-(d:documento) WHERE d.regione="campania" WITH n MATCH (n)-[r]-(m) WHERE (n.tag <> "individuo" AND n.tag <> "gruppo") RETURN r'
#
#     # Use the datamanager module to get the graph and sentences
#     dm  = datamanager.datamanager()
#     ds  = dm.getGraph(query=Q_OLD_CAMPANIA, usecache=False)
#
#     dt   = datamanager.datatransform()
#     uds  = dt.makeUndirected(ds)
#     dt.netStats(uds, outfile)

def printstats(outfile, neo4j_connection):
    res = {
        'persona': 0,
        'organizzazione': 0
    }
    for tag in res.keys():
        q = 'MATCH (n:%s) RETURN COUNT(n)' % tag
        for p in neo4j_connection.cursor().execute(q):
            res[tag] = p[0]
    return res

def mongoGetConnection():
    try:
        neo4j_connection = datamanager_legacy.neo4jInit()
        return neo4j_connection
    except Exception as e:
        print("Neo4j error: %s" % str(e))
        sys.exit(1)


def main(argv):
    plotMatrix = False
    saveDendrogram = False

    mergiableTags = ['persona', 'organizzazione']
#    mergiableTags = ['organizzazione']
    tags = []

    if len(argv) == 1: # all tags
        tags = mergiableTags
    else:
        tags = [t for t in argv[1:] if t in mergiableTags]

    neo4j_connection = mongoGetConnection()

    if(len(argv) > 1):
        if(argv[1] == "-i"):
            #creo indicizzazione
            q = "CREATE INDEX ON :persona(internal_id)"
            for p in neo4j_connection.cursor().execute(q):
                print ("Index", p)
    else:
        log.info("Indicizzazione non necessaria.")

    log.info("Calculate statistics before processing the graph...")
    res = printstats('results/node_stats_before_processing.out', neo4j_connection)
    log.info("|persone|=%d |organizzazioni|=%d" % (res['persona'], res['organizzazione']))
    neo4j_connection.close()

    for tag in tags:
        log.info("Merge tag: %s" % tag)

        neo4j_connection = mongoGetConnection()
        nodes = mergeUtils.getNodes(neo4j_connection, tag=tag)
        cacheKey = json.dumps([n['id'] for n in nodes])
        log.info("Nodes found: %d" % len(nodes))
        neo4j_connection.close()

        log.info("Calculate fuzzy distances with multiprocessing method...")
        start_time = time.time()
        distanceMatrix = cacheManager.cache(cacheKey, mergeUtils.calculateDistanceMultiproc, nodes, tag)
        log.info("Done in %s seconds" % (time.time() - start_time))

        if plotMatrix:
            log.info("Plotting the distance matrix...")
            mergeUtils.plot(distanceMatrix, nodes, 'Fuzzy distance', 'results/fuzzy_distance_Campania_%s.pdf' % tag)
        else:
            log.info("Skipping the plotting of the distance matrix")

        if saveDendrogram:
            log.info("Saving dendrogram figure...")
            mergeUtils.clusteringDendrogramPlot(distanceMatrix, nodes, threshold=20, filename='results/Dendrogram_%s.pdf' % tag)
        else:
            log.info("Skipping the saving of the dendrogram figure")

        log.info("Clustering...")
        clusters = mergeUtils.clustering(distanceMatrix, nodes, tag, threshold=20, min_cluster_items=2)

        log.info("Merging...")
        neo4j_connection = mongoGetConnection()
        mergeUtils.mergeNodesInClusters(clusters, distanceMatrix, neo4j_connection, mergeThreshold=10, apply=False)
        neo4j_connection.close()

    neo4j_connection = mongoGetConnection()
    log.info("Calculate statistics before processing the graph...")
    res = printstats('results/node_stats_before_processing.out', neo4j_connection)
    log.info("|persone|=%d |organizzazioni|=%d" % (res['persona'], res['organizzazione']))
    log.info("Done (%s seconds)" % (time.time() - start_time))
    neo4j_connection.close()


if __name__ == "__main__":
    main(sys.argv)
