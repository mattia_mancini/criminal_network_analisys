import json
import os
import re
import sys

import xmltodict

from lib import datamanager_legacy, logger, geoUtils

log = logger.getLogger(__file__)

internal_node_id = 0

def flattenEntity(entity):
    partialKeyMap = []
    for (key, val) in entity.items():

        if val is None:
            continue
        if isinstance(val, str) and val.isdigit():
            value = "%s: %d" % (key, int(val))
        elif isinstance(val, int):
            value = "%s: %d" % (key, val)
        else:
            value = "%s: %s" % (key, json.dumps(val))

        partialKeyMap.append(value)

    return ', '.join(partialKeyMap)


def buildInsertString(padre, figlio, link):
    '''
    Neo4j does not support undirected relationships.
    Neo4j APIs allow developers to completely ignore relationship direction when querying the graph, if they so desire.
    The correct (or at least most efficient) way of modelling the partner relationships is using a single PARTNER relationship with an arbitrary direction.
    (see http://graphaware.com/neo4j/2013/10/11/neo4j-bidirectional-relationships.html)
    '''
    if 'descrizionelink' in link:
        link['tag'] = link['descrizionelink']\
            .strip()\
            .replace(" ", "_")\
            .replace(".", "")

    if 'descrizionelink' in link: del link['descrizionelink']
    if 'padreid' in link:
        del link['padreid']
    if 'figlioid' in link:
        del link['figlioid']

    # workaround per cyNeo4j: il tool di importazione in cytoscape sembra non supportare le label dei nodi
    padre['entity']['tag'] = padre['tag']
    figlio['entity']['tag'] = figlio['tag']

    padreEntity = "MERGE (sFrom:%s {%s})" % (padre['tag'], flattenEntity(padre['entity']))
    figlioEntity = "MERGE (sTo:%s {%s})" % (figlio['tag'], flattenEntity(figlio['entity']))
    linkEntity = "MERGE (sFrom)-[:%s {%s}]->(sTo)" % (link['tag'], flattenEntity(link))

    return "%s %s %s" % (padreEntity, figlioEntity, linkEntity)

def getRegionFromFilename(file):
    basename = os.path.basename(file)
    m = re.search('reg_([^_]+)_.*', basename)
    try :
        if len(m.groups()) > 0:
            region = m.group(1)
        else:
            region = None
    except :
        region = None
    return region

def importFile(file):
    global log, neo4j_connection, internal_node_id

    log.info("Importing file %s:" % file)

    links = []
    allentities = {}
    countEntities = 0
    doc = xmltodict.parse(open(file, 'rb'))

    drereference = doc['DOCUMENT']['DREREFERENCE']
    dretitle = 'Senza nome'

    if 'DRETITLE' in doc['DOCUMENT']: dretitle = doc['DOCUMENT']['DRETITLE']


    appearsInDocumentLink = {
        'tag':  'appears_in_document'
    }

    documentEntity = {
        'tag':                      'documento',
        'entity':   {
            'drereference':         drereference,
            'dretitle':             dretitle,
            'internal_id':          internal_node_id
        }
    }

    # Region extraction from filename
    region = getRegionFromFilename(file)
    if region:
        documentEntity['entity']['regione'] = region

    internal_node_id += 1

    cursor = neo4j_connection.cursor()
    for tag, entities in list(doc['DOCUMENT']['DREFIELD']['ENTITIES'].items()):
        if tag == "documento":
            continue
        if tag == "link":
            links = entities
            for l in links:
                l['drereference'] = drereference
        else:

            if isinstance(entities, list):
                for entity in entities:
                    entity['drereference'] = drereference
                    entity['internal_id'] = internal_node_id
                    allentities[int(entity['entitaid'])] = {
                        'tag':      tag,
                        'entity':   dict(list(entity.items()))
                    }
                    countEntities += 1
                    internal_node_id += 1
                    cursor.execute(buildInsertString(allentities[int(entity['entitaid'])], documentEntity, appearsInDocumentLink))
            else:
                entities['drereference'] = drereference
                entities['internal_id'] = internal_node_id
                allentities[int(entities['entitaid'])] = {
                    'tag':      tag,
                    'entity':   dict(list(entities.items()))
                }
                countEntities += 1
                internal_node_id += 1
                cursor.execute(buildInsertString(allentities[int(entities['entitaid'])], documentEntity, appearsInDocumentLink))

    for link in links:
        idPadre = int(link['padreid'])
        idFiglio = int(link['figlioid'])

        if idPadre not in allentities:
            log.warn('Entity %d was not present!', idPadre)
            continue

        if idFiglio not in allentities:
            log.warn('Entity %d was not present!', idFiglio)
            continue

        padre = allentities[idPadre]
        figlio = allentities[idFiglio]

        mergeString = buildInsertString(padre, figlio, link)
        cursor.execute(mergeString)

    neo4j_connection.commit()
    log.info("Imported nodes %s, imported links: %s" % (countEntities, len(links)))

def getNeoConnection():
    try:
        return datamanager_legacy.neo4jInit()
    except Exception as e:
        log.error("Neo4j error: %s" % str(e))
        sys.exit()

def main(argv):
    global log, neo4j_connection, internal_node_id

    if len(argv) < 2:
        print('XML-data-loader.py <xml|directory>')
        sys.exit(2)

    path = argv[1]
    files = []

    if os.path.isdir(path):
        for file in os.listdir(path):
            if file.endswith(".xml") or file.endswith(".idx"):
                files.append(path + "/" + file)
    else:
        files = [path]

    neo4j_connection = getNeoConnection()
    datamanager_legacy.deleteAll(neo4j_connection)
    neo4j_connection.close()

    neo4j_connection = getNeoConnection()
    for max, in neo4j_connection.cursor().execute("MATCH (n) WITH max(n.internal_id) as max RETURN max"):
        if max:
            internal_node_id = max + 1
    neo4j_connection.close()

    neo4j_connection = getNeoConnection()
    for file in files:
        importFile(file)
    neo4j_connection.close()

    # Geo-Localize all data
    neo4j_connection = getNeoConnection()
    nodes = geoUtils.getNodesWithoutCoordinates(neo4j_connection)
    log.info("Nodes to be geocoded: %d" % len(nodes))
    geoUtils.geocode(nodes, neo4j_connection)
    neo4j_connection.close()

if __name__ == "__main__":
    main(sys.argv)
